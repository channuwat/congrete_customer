// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAscilrqRznimU8mwoLrufXfvo8m2VnQDc",
    authDomain: "concretedeliveryeasy-f16ad.firebaseapp.com",
    databaseURL: "https://concretedeliveryeasy-f16ad.firebaseio.com",
    projectId: "concretedeliveryeasy-f16ad",
    storageBucket: "concretedeliveryeasy-f16ad.appspot.com",
    messagingSenderId: "472114082705",
    appId: "1:472114082705:web:ba657314a370fce3936030",
    measurementId: "G-GV7WW66GQD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
