import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Step21PageRoutingModule } from './step21-routing.module';

import { Step21Page } from './step21.page';
import { CalculateConcreteVolumePage } from '../calculate-concrete-volume/calculate-concrete-volume.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    Step21PageRoutingModule
  ],
  declarations: [Step21Page],
})
export class Step21PageModule { }
