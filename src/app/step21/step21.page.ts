import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, AlertController, IonInput } from '@ionic/angular';
import { CalculateConcreteVolumePage } from '../calculate-concrete-volume/calculate-concrete-volume.page';
import { NgModel, FormGroup, FormControl, Validators } from '@angular/forms';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { Toast } from '@capacitor/core';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-step21',
  templateUrl: './step21.page.html',
  styleUrls: ['./step21.page.scss'],
})

export class Step21Page implements OnInit {
  @ViewChild('date', { static: false }) dateInput: IonInput;
  h_auto = false;
  public step2 = {
    p_name: '',
    p_img: '',
    how_to_use: ''
  };
  public show_aboutus: boolean = true;
  public aboutus = {
    policy: '',
    service: ''
  };
  public date_send: any = 0;
  public time_sent: any = 0;
  public dateTime2: any;
  public count_true = false;
  public formStep21: FormGroup;
  constructor(public router: Router,
    public api: WebapiServiceProvider,
    public alertController: AlertController,
    private ref: ChangeDetectorRef,
    public translate: TranslateService,
    public modalController: ModalController) {

  }
  agreee = '1'
  change_agree(e) {

    if (e.detail.checked) {
      this.agreee = '1';
      console.log(this.agreee);
    } else {
      this.agreee = '0';
      console.log(this.agreee);
    }

  }
  public isActive = false
  dateClick() {
    this.isActive = true
    setTimeout(() => {
      this.dateInput.setFocus()
    }, 300);
  }
  ngOnInit() {
    console.log('Oninit');
    // var element = <HTMLInputElement> document.getElementById("agreeCheck");
    // console.log('Checkbox = '+element.checked);

    this.api.storage_get("step2").then((step2) => {
      this.step2 = step2;
    })
    // let dd = new Date().getDate()
    // let mm = new Date().getMonth()+1
    // let YYYY = new Date().getFullYear()
    // this.dateTimeNow = dd+'-'+mm+'-'+YYYY
    // let today = new Date(this.dateTimeNow)
    this.formStep21 = new FormGroup({
      date: new FormControl('', [Validators.required]),
      time: new FormControl('', [Validators.required]),
      count: new FormControl('', [Validators.required]),

    })
    this.load_policy()
  }
  public timeError = false;
  public dateTimeNow = '2020-01-01'
  dateChange(event) {
    this.date_send = event.target.value
    let dd = new Date().getDate()
    let mm = new Date().getMonth() + 1
    let YYYY = new Date().getFullYear()
    this.dateTimeNow = YYYY + '-' + mm + '-' + dd
    let today = new Date(this.dateTimeNow)
    let another = new Date(this.date_send)
    if (another < today) {
      console.log('Do not cloose send date is yesterday!!');
      this.timeErr();
      this.formStep21.patchValue({
        date: '',
        time: ''
      });
      this.timeError = true
    } else {
      this.timeError = false
      var data = this.formStep21.value;
      this.api.storage_set("step21", data);

    }
  }
  async timeErr() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      mode: 'ios',
      header: 'วันที่และเวลาจัดส่งไม่ถูกต้อง',
      message: 'วันที่และเวลาจัดส่งต้องมากกว่า 3 ชั่วโมง กรุณาระบุใหม่อีกครั้ง',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }
  timeChange(event) {
    this.time_sent = event.target.value
    this.dateTime2 = new Date(this.date_send + ' ' + event.target.value);
    console.log(this.dateTime2);
    let timeNow = new Date()
    timeNow.setHours(timeNow.getHours() + 3);

    if (this.dateTime2 < timeNow) {
      this.timeErr();
      this.formStep21.patchValue({
        time: '',
      });
      this.timeError = true
    } else {
      this.timeError = false
      var data = this.formStep21.value;
      this.api.storage_set("step21", data);
    }
  }

  back() {
    this.router.navigate(['/main/step2']);
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      mode: 'ios',
      header: "จำนวนคิวไม่ถูกต้อง",
      // subHeader: 'Subtitle',
      message: 'ต้องเป็นจำนวนนับ หรือ ทศนิยม .25 .5 .75 เท่านั้น',
      buttons: ['OK']
    });

    await alert.present();
  }
  check_q(e) {
    let a = e.target.value * 100 % 25;
    if (a != 0 || e.target.value < 1) {
      this.count_true = false;
      this.presentAlert();
    } else {
      this.count_true = true;
      var data = this.formStep21.value;
      this.api.storage_set("step21", data);
    }


  }
  next() {
    var d1 = new Date();
    var d = new Date(d1.getFullYear() + "-" + (d1.getMonth() + 1) + "-" + d1.getDate());
    var date = d.getTime();
    var data: any = this.formStep21.value;
    var d2 = new Date(data.date);
    let dateTime = new Date().getTime();
    if (!this.timeError) {
      // console.log('OK OK OK');
      // this.api.Toast("โปรดระบุเวลาอย่างน้อย 3 ชั่วโมง หลังเปิดประมูล");
      console.log("success");
      this.api.storage_set("step21", data);
      setTimeout(() => {
        this.router.navigate(['/main/step3']);
      }, 100);

    } else {
      this.api.Toast('วันที่และเวลาไม่ถูกต้อง กรุณาระบุใหม่อีกครั้ง')
    }

  }
  agreement() {
    if (this.show_aboutus) {
      this.show_aboutus = false;
    } else {
      this.show_aboutus = true;
    }

  }
  load_policy() {
    this.api.getData('load_policy').then((res: any) => {
      if (res) {
        this.aboutus = res;
      }

    })
  }

  open_date() {
    // var options = {
    //   type: 'date',         // 'date' or 'time', required
    //   date: new Date(),     // date or timestamp, default: current date
    //   minDate: new Date(),  // date or timestamp
    //   maxDate: new Date()   // date or timestamp
    // };

    // window.DateTimePicker.pick(options, function (timestamp) {
    //   window.alert(timestamp);
    // });
  }
  calculate() {
    this.router.navigate(['/main/cal-concrete']);

  }
  async calculate2() {

    const modal = await this.modalController.create({
      component: CalculateConcreteVolumePage
    });
    modal.onDidDismiss()
      .then((data) => {
        console.log(data);
      });
    return await modal.present();
  }
  show_more() {
    this.h_auto = true;
  }
  async doRefresh(event) {
    await this.ngOnInit();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }
  test(e) {
    console.log(e);
  }
  public lang = 'th';
  ionViewWillEnter() {
    this.api.storage_get('lang').then((res: any) => {
      this.lang = res;
    })
    this.api.storage_get('step21').then((res: any) => {
      if (res == null) {
        this.formStep21 = new FormGroup({
          date: new FormControl('', [Validators.required]),
          time: new FormControl('', [Validators.required]),
          count: new FormControl('', [Validators.required]),

        })
        this.isActive = false
      } else {

      }
    })
    this.api.storage_get("step2").then((step2) => {
      this.step2 = step2;
      setTimeout(() => {
        this.ref.detectChanges();
      }, 200);
    })
    this.ref.detectChanges();
    console.log('ionViewWillEnter');
  }
}
