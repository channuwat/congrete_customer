import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ModalController, AlertController,LoadingController } from '@ionic/angular';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { TranslateService } from '@ngx-translate/core';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { Router } from '@angular/router';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  @Input() order: any = {
    address: "",
    admin: "",
    ap_id: "",
    car: "",
    count: "",
    cre_date: "",
    district: "",
    end_date: "",
    member_id: "",
    name: "",
    num_diff: '',
    order_id: "",
    p_id: "",
    p_name: { th: "", en: "" },
    province: "",
    rate: "",
    rate_id: "",
    receive: "",
    receive_name: "",
    receive_phone: "",
    send_date: "",
    send_time: "",
    status: "",
    subdistrict: "",
    zip_code: ""
  }
  public bank = {
    0: {
      bb_id: '',
      no: '',
      name: '',
      bank: ''
    },
    1: {
      bb_id: '',
      no: '',
      name: '',
      bank: ''
    }
  }
  public selected_bank = '';
  public winner = {
    name: '',
    sumprice: 0
  }
  public payment = {
    status: '0',
    bb_id: ''
  }
  public fbstatus = false;
  public lang = 'th'
  constructor(
    public modal: ModalController,
    public translate: TranslateService,
    public ref: ChangeDetectorRef,
    public api: WebapiServiceProvider,
    public alertController: AlertController,
    public loadingController: LoadingController,
    private clipboard: Clipboard,
    public rout : Router
  ) {
    this.api.storage_get('lang').then((res: any) => {
      if (res == 'en') {
        this.lang = 'en'
      }else{
        this.lang = 'th'
      }
    })
  }
  public data = {
    member_id: ''
  }
  rowClick(bb_id){
    this.selected_bank = bb_id;
    this.ref.detectChanges();
  }
  public score = 0;
  ionViewWillEnter() {
    console.log(this.order);
    this.api.storage_get('data_login').then((data: any) => {
      this.data = data;
      this.api.getData('get_score/' + this.data.member_id).then((res: any) => {
        console.log(res);
        this.score = res.score;
      })
    });
  }

  ngOnInit() {
    this.loadAuctionList()
    this.load_bank();
    this.load_winner(this.order)
    this.load_payment();
    console.log(this.order);
  }
  get_bank() {
    this.load_bank();
  }
  public order_data: any = {
    address: "",
    admin: "",
    ap_id: "",
    car: "",
    count: "",
    cre_date: "",
    district: "",
    end_date: "",
    member_id: "",
    name: "",
    num_diff: '',
    order_id: "",
    p_id: "",
    p_name: { th: "", en: "" },
    province: "",
    rate: "",
    rate_id: "",
    receive: "",
    receive_name: "",
    receive_phone: "",
    send_date: "",
    send_time: "",
    status: "",
    subdistrict: "",
    zip_code: ""
  }
  loadAuctionList() {
    console.log('loadAuctionList');    
    this.api.getData('get_vs_auction/' + this.order).then((data: any) => {
      console.log(data);
      this.order_data = data.order_data
    })
  }
  load_payment() {
    this.api.getData('get_payment/' + this.order).then((res: any) => {
      console.log(res);

      if (!res) {
        this.success = false;
        this.payment.status = '0';
      } else if (res.status == '1') {
        this.success = true;
        this.url = res.picture;
        this.totalprice = res.total;
        this.payment = res;
        this.selected_bank = res.bb_id;
      } else if (res.status == '-2') {
        this.success = false;
        this.url = res.picture;
        this.totalprice = res.total;
        this.payment = res;
        this.selected_bank = res.bb_id;
      } else if (res.status == '0') {
        this.success = true;
        this.url = res.picture;
        this.payment = res;
        this.totalprice = res.total;
        this.selected_bank = res.bb_id;
      }
      // this.url = res.picture
      // if (res.status == 1) {
      //   this.success = true;
      // }else if (res == null){
      //   this.success = false;
      //   this.payment.status = 0;
      // }
      console.log(this.success);
      
    })
  }
  load_bank() {
    this.api.getData('get_bank').then((res: any) => {
      console.log(res);
      this.bank = res
      this.selected_bank=res[0].bb_id

    })
  }
  load_winner(order_id) {
    this.api.getData('who_won/' + order_id).then((res: any) => {
      console.log(res);
      this.winner = res;
      this.totalprice = res.sumprice
    })
  }

  public copyMessage(val: string) {
    this.clipboard.copy(val);
    // const selBox = document.createElement('textarea');
    // selBox.style.position = 'fixed';
    // selBox.style.left = '0';
    // selBox.style.top = '0';
    // selBox.style.opacity = '0';
    // selBox.value = val;
    // document.body.appendChild(selBox);
    // selBox.focus();
    // selBox.select();
    // document.execCommand('copy');
    // document.body.removeChild(selBox);
    this.api.Toast('คัดลอกเลขบัญชีธนาคาร ' + val)
  }
  public success = true;
  public imagePath: any = '';
  public url = '';
  public reader: any;
  public type: any;
  async add_slip(event) {
    event.preventDefault();
    let element: HTMLElement = document.getElementById('slipinput') as HTMLElement;
    element.click();
  }
  public formData = new FormData();
  changeListener(event): void {
    this.formData.append('avatar', event.target.files[0]);
    this.reader = new FileReader();
    this.reader.onload = (event: any) => {
      console.log(event);
      this.url = event.target.result;
      // console.log(this.url);
      this.ref.detectChanges()

    }
    this.reader.readAsDataURL(event.target.files[0]);
    this.type = event.target.files[0].type;
    this.ref.detectChanges()
  }
  continue() {
    console.log(this.selected_bank);
    if (this.selected_bank == '') {
      this.api.Toast('กรุณาเลือกบัญชี')
    } else if (this.url == '') {
      this.api.Toast('กรุณาแนบหลักฐานการโอน')
    } else {
      this.presentUpload()
    }
  }
  async presentUpload() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'กำลังอัพโหลด...'
    });
    await loading.present();
    this.api.postData('upload', { data: this.reader.result, type: this.type, order_id: this.order, bb_id: this.selected_bank, price: this.totalprice, real_price: this.winner.sumprice,member_id:this.data.member_id }).then(async(res: any) => {
      console.log('upload success!!');
      await loading.dismiss();
      this.presentAlert()
      this.api.fb_set('order');
      this.load_payment();
    })
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      mode:'ios',
      header: 'บันทึกเรียบร้อย',
      message: 'หลังจากอัพโหลดเรียบร้อยแล้ว ระบบจะใช้เวลาสักครู่เพื่อตรวจสอบ',
      buttons: [
        {
        text: 'OK',
        handler: () => {
          console.log('OK');
          this.close()
        }
      },
      ]
    });

    await alert.present();
    
  }

  close() {
    console.log('close');
    this.modal.dismiss()
  }
  public totalprice: number = 0;
  async discount(score) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      mode:'ios',
      header: 'ใช้คะแนนทั้งหมด',
      message: 'ต้องการใช้คะแนนทั้งหมด <br> จำนวน <strong>' + score + '</strong> คะแนน <br> หลังจากใช้คะแนนจะยังสามารถรับคะแนนในคำสั่งซื้อนี้',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            this.totalprice -= score;
            if (this.totalprice < 0) {
              this.totalprice = 0;
            }
            this.ref.detectChanges()
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
  contact(){
    this.close()
    setTimeout(() => {
      this.rout.navigate(['/main/tab3']);
    }, 100);
  }
}
