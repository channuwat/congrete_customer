import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentPageRoutingModule } from './payment-routing.module';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { PaymentPage } from './payment.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxIonicImageViewerModule,
    PaymentPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [PaymentPage]
})
export class PaymentPageModule {}
