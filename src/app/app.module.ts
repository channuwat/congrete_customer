
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ComponentsModule } from './components/components.module';

import { ServiceWorkerModule } from '@angular/service-worker';
import { Facebook } from '@ionic-native/facebook/ngx';
import { environment } from '../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NotificationDetailPage } from './notification-detail/notification-detail.page';
import { IonicStorageModule } from '@ionic/storage';
import { WebapiServiceProvider } from './providers/webapi-service/webapi-service';
import { IonicSelectableModule } from 'ionic-selectable';
import { ProvincePage } from './province/province.page';
import { AngularFireModule } from '@angular/fire';
import { FCM } from '@ionic-native/fcm/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Device } from '@ionic-native/device/ngx';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AddCarPage } from './add-car/add-car.page';
import { AddCarPageModule } from './add-car/add-car.module';
import { NotificationDetailPageModule } from './notification-detail/notification-detail.module';
import { CalculateConcreteVolumePage } from './calculate-concrete-volume/calculate-concrete-volume.page';
import { CalculateConcreteVolumePageModule } from './calculate-concrete-volume/calculate-concrete-volume.module';
import { ProvincePageModule } from './province/province.module';
import { PrivacyPolicyPage } from './privacy-policy/privacy-policy.page';
import { TermsOfServicePage } from './terms-of-service/terms-of-service.page';
import { ConfigAccountPageModule } from './config-account/config-account.module';
import { ScorePageModule } from './score/score.module';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { AuctionCongretePage } from './auction-congrete/auction-congrete.page';
import { AuctionCongretePageModule } from './auction-congrete/auction-congrete.module';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility/ngx';
import { AddPasswordPage } from './add-password/add-password.page';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { NotiDetailsPage } from './noti-details/noti-details.page';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    TermsOfServicePage,
    PrivacyPolicyPage,
    AppComponent,
    ProvincePage,
    AddCarPage,
    AddPasswordPage,
    NotificationDetailPage,
    NotiDetailsPage,
    CalculateConcreteVolumePage,
  ],
  entryComponents: [
    ProvincePage,
    AuctionCongretePage,
    AddPasswordPage,
    NotiDetailsPage
  ],
  imports: [
    IonicModule.forRoot(),
    BrowserModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    IonicSelectableModule,
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    ComponentsModule,
    AddCarPageModule,
    AuctionCongretePageModule,
    ScorePageModule,
    ConfigAccountPageModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    CalculateConcreteVolumePageModule,
    NotificationDetailPageModule,
    // SocialSharing,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })

  ],
  providers: [
    WebapiServiceProvider,
    Facebook,
    Network,
    FCM,
    Device,
    Clipboard,
    SplashScreen,
    SocialSharing,
    MobileAccessibility,
    YoutubeVideoPlayer,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
