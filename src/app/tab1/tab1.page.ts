import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController, IonSelect, IonInput } from '@ionic/angular';
import { AddCarPage } from '../add-car/add-car.page';
import { Router } from '@angular/router';
import { IonicSelectableComponent } from 'ionic-selectable';
import { ProvincePage } from '../province/province.page';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { TranslateService } from '@ngx-translate/core';
class Port {
  public id: number;
  public name: string;
}

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  step1: FormGroup;
  ports: Port[];
  port: Port;
  district = [];
  province = { name_th: '',name_en:'' };
  subdistrict = [];
  public district_one = {};
  public subdistrict_one = {};
  validation_messages = {
    province: [
      { type: 'required', message: 'เลือกจังหวัดก่อน' },
    ],
    subdistrict: [
      { type: 'required', message: 'เลือกแขวง/ตำบลก่อน' },
    ],
    district: [
      { type: 'required', message: 'เลือกเขต/อำเภอก่อน' },
    ],
    address: [
    ]
  };

  @ViewChild('district_select', { static: false }) district_select: IonSelect;
  @ViewChild('subdistrict_select', { static: false }) subdistrict_select: IonSelect;
  @ViewChild('addressinput', { static: false }) addressinput: IonInput;
  constructor(
    public modalController: ModalController, 
    public rout: Router, 
    public api: WebapiServiceProvider,
    public translate: TranslateService,) {
    this.ports = [
      { id: 1, name: 'Tokai' },
      { id: 2, name: 'Vladivostok' },
      { id: 3, name: 'Navlakhi' }
    ];


    this.step1 = new FormGroup({
      'province': new FormControl('', Validators.compose([
      ])),
      'district': new FormControl('', Validators.compose([
      ])),
      'subdistrict': new FormControl('', Validators.compose([
        Validators.required
      ])),
      'address': new FormControl('', Validators.compose([
        Validators.required
      ])),
      'car': new FormControl(2),
      'receive': new FormControl(false),
      'receive2': new FormControl(true),
      'receive_name': new FormControl(''),
      'receive_phone': new FormControl('')
    });
  }
  public lang='th'
  ngOnInit() {
    this.api.storage_get('lang').then((res:any)=>{
      this.lang=res
    })
    setTimeout(() => {
      // this.select_province();
    }, 2000);
  }
  recevie1_change() {
    setTimeout(() => {
      this.step1.patchValue({ receive: true, receive2: false });
      console.log(this.step1.value);
    }, 10);


    console.log('1');
  }
  recevie2_change() {
    setTimeout(() => {

      this.step1.patchValue({ receive: false, receive2: true });
      console.log(this.step1.value);
    }, 10);
    console.log('2');
  }
  portChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
  }
  doLogin() {
  }
  district_change() {
    this.subdistrict.forEach(element => {
      if (element.id == this.step1.value.subdistrict) {
        this.subdistrict_one = element;
      }
    })
    setTimeout(() => {
      this.addressinput.setFocus();
    }, 500);
  }
  is_disabled() {
    if (this.step1.value.receive2) {
      if (this.step1.value.receive_name == '' || this.step1.value.receive_phone == '') {
        return true;
      }
    }
    if (this.step1.value.car == '') {
      return true;
    }
    return false;
  }
  async select_car() {
    const modal = await this.modalController.create({
      component: AddCarPage
    });
    modal.onDidDismiss()
      .then((data) => {
        console.log(data);
        this.step1.patchValue({ car: data.data.type });
      });

    return await modal.present();

  }
  add_enter() {
    this.select_car();
  }
  back() {
    this.rout.navigate(['/main/home'])
    this.api.storage_set("step1", null);
    this.api.storage_set("step2", null);
    this.api.storage_set("step21", null);
  }
  next() {
    var step1 = this.step1.value;
    step1.province = this.province;
    step1.district_one = this.district_one;
    step1.subdistrict_one = this.subdistrict_one;
    this.api.storage_set("step1", step1);
    setTimeout(() => {
      this.rout.navigate(['/main/step2'])
    }, 100);
  }
  load_subdistrict() {

    this.district.forEach(element => {
      if (element.id == this.step1.value.district) {
        this.district_one = element;
        console.log(this.district_one);
      }
    });
    this.api.getData('get_subdistrict/' + this.step1.value.district).then((res: any) => {
      this.subdistrict = res;
      this.subdistrict_select.open();
    });
  }
  load_district(province_id) {

    this.api.getData("get_district/" + province_id).then((res: any) => {
      this.district = res;
      this.district_select.open();
    })
  }

  async select_province() {
    const modal = await this.modalController.create({
      component: ProvincePage
    });
    modal.onDidDismiss().then((res: any) => {
      console.log(res);
      if (res.data) {
        this.province = res.data;

        this.load_district(res.data.id);
      }
    });
    return await modal.present();

  }
  async doRefresh(event) {
    await this.ngOnInit();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }

  ionViewWillEnter(){
    this.api.storage_get('step1').then((res:any)=>{
      if (res == null) {
        this.province = { name_th: '',name_en:'' };
        this.step1 = new FormGroup({
          'province': new FormControl('', Validators.compose([
          ])),
          'district': new FormControl('', Validators.compose([
          ])),
          'subdistrict': new FormControl('', Validators.compose([
            Validators.required
          ])),
          'address': new FormControl('', Validators.compose([
            Validators.required
          ])),
          'car': new FormControl(2),
          'receive': new FormControl(false),
          'receive2': new FormControl(true),
          'receive_name': new FormControl(''),
          'receive_phone': new FormControl('')
        });
      }
    })
  }

}
