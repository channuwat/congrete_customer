import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-confirm-password',
  templateUrl: './confirm-password2.page.html',
  styleUrls: ['./confirm-password2.page.scss'],
})
export class ConfirmPassword2Page implements OnInit {
  signupForm: FormGroup;
  matching_passwords_group: FormGroup;


  validation_messages = {
    "otp1": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp2": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp3": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp4": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp5": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp6": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],

  };
  public code = ''; l
  constructor(
    public router: Router, 
    public translate: TranslateService,
    public api: WebapiServiceProvider
    ) {
    api.storage_get('otp_code').then((code) => {
      console.log(code);
      this.code = code;
    });
    this.signupForm = new FormGroup({
      otp1: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp2: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp3: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp4: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp5: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp6: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
    });
  }
  doSignup() {
    console.log(this.signupForm.value);
    this.api.storage_get('otp_phone').then((val: any) => {
      let otp = this.signupForm.value.otp1 + this.signupForm.value.otp2 + this.signupForm.value.otp3 + this.signupForm.value.otp4 + this.signupForm.value.otp5 + this.signupForm.value.otp6
      this.api.postData('confirm_otp', { otp:otp, tel: val }).then((result: any) => {
        console.log(result);
        if (result.flag == '1') {
          this.router.navigate(['/auth/change-password2']);
        } else {
          this.api.Toast("รหัสไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง หากยังไม่ได้รับให้กดส่งรหัสอีกครั้ง")
        }
      })
    });


  }
  ngOnInit() {
  }
  send_agen() {
    this.api.storage_get('otp_phone').then((val: any) => {
      this.api.postData('add_otp', { tel: val, type: 2 }).then((res: any) => {
        console.log(res);
        if (res.flag == '0') {
          this.api.Toast(res.error);
        } else {
          this.api.Toast("ระบบได้ส่ง SMS ไปยังเบอร์ของคุณเรียบร้อยแล้ว");

        }
      });
    });
  }
  otpController(event, next, prev) {
    if (event.target.value.length < 1 && prev) {
      prev.setFocus()
    }
    else if (next && event.target.value.length > 0) {
      next.setFocus();
    }
    else {
      return 0;
    }
  }
}
