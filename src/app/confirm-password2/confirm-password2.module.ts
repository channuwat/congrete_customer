import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmPassword2PageRoutingModule } from './confirm-password2-routing.module';

import { ConfirmPassword2Page } from './confirm-password2.page';
import { ComponentsModule } from '../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    ConfirmPassword2PageRoutingModule,
    TranslateModule.forChild()

  ],
  declarations: [ConfirmPassword2Page]
})
export class ConfirmPassword2PageModule {}
