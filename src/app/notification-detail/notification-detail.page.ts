import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.page.html',
  styleUrls: ['./notification-detail.page.scss'],
})
export class NotificationDetailPage implements OnInit {
  public data = { title: '', content: '' }
  constructor(
    public modalCtrl: ModalController,
    public translate: TranslateService,
    // , private navParams: NavParams
    ) { }

  ngOnInit() {
    // this.data = this.navParams.get("data");
  }
  back() {
    this.modalCtrl.dismiss();

  }

}
