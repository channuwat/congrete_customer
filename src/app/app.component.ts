import { Component, OnInit, ChangeDetectorRef, ViewChildren } from '@angular/core';
import { Plugins } from '@capacitor/core';
// import { SplashScreen } from '@ionic-native/splash-screen';
// const { SplashScreen } = Plugins;
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { FCM } from '@ionic-native/fcm/ngx';
import { Platform, ModalController, AlertController, IonRouterOutlet, ToastController, ActionSheetController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network/ngx';
import { WebapiServiceProvider } from './providers/webapi-service/webapi-service';
import { Device } from '@ionic-native/device/ngx';
import { Router } from '@angular/router';
import { AuctionCongretePage } from './auction-congrete/auction-congrete.page';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: [
    './side-menu/styles/side-menu.scss',
    './side-menu/styles/side-menu.shell.scss',
    './side-menu/styles/side-menu.responsive.scss'
  ]
})

export class AppComponent implements OnInit {
  subscribe: any;
  appPages = [
    {
      title: 'Categories',
      url: '/app/categories',
      icon: './assets/sample-icons/side-menu/categories.svg'
    },
    {
      title: 'Profile',
      url: '/app/user',
      icon: './assets/sample-icons/side-menu/profile.svg'
    },
    {
      title: 'Contact Card',
      url: '/contact-card',
      icon: './assets/sample-icons/side-menu/contact-card.svg'
    },
    {
      title: 'Notifications',
      url: '/app/notifications',
      icon: './assets/sample-icons/side-menu/notifications.svg'
    }
  ];

  accountPages = [
    {
      title: 'Log In',
      url: '/auth/login',
      icon: './assets/sample-icons/side-menu/login.svg'
    },
    {
      title: 'Sign Up',
      url: '/auth/signup',
      icon: './assets/sample-icons/side-menu/signup.svg'
    },
    {
      title: 'Tutorial',
      url: '/walkthrough',
      icon: './assets/sample-icons/side-menu/tutorial.svg'
    },
    {
      title: 'Getting Started',
      url: '/getting-started',
      icon: './assets/sample-icons/side-menu/getting-started.svg'
    },
    {
      title: '404 page',
      url: '/page-not-found',
      icon: './assets/sample-icons/side-menu/warning.svg'
    }
  ];

  textDir = 'th';
  userDeviceData = { uuid: "", token: "", platform: "", version: "", member_id: '0' };
  userDeviceResponse: any;
  @ViewChildren(IonRouterOutlet) routerOutlet: IonRouterOutlet;
  constructor(
    public translate: TranslateService,
    public rout: Router,
    private fcm: FCM,
    public alert: AlertController,
    private storage: Storage,
    private platform: Platform,
    public device: Device,
    private splashScreen: SplashScreen,
    public network: Network,
    private webService: WebapiServiceProvider,
    public modalController: ModalController,
    private toast: ToastController,
    public actionSheetController: ActionSheetController,
    private mobileAccessibility: MobileAccessibility
  ) {
    
    this.initializeApp();
    this.setLanguage();
    this.mobileAccessibility.usePreferredTextZoom(false);
    this.backButtonEvent();
    if (this.platform.is('android') === true) {

      this.platform.ready().then(() => {

        this.mobileAccessibility.usePreferredTextZoom(false);
        setTimeout(() => {
          this.fcmNotify();
        }, 100);
        this.checkData();
      });
    }
  }
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  backButtonEvent() {
    this.subscribe = this.platform.backButton.subscribeWithPriority(666666, async () => {

      try {
        const element = await this.alert.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);

      }
      try {
        const element = await this.actionSheetController.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);

      }
      try {
        const element = await this.modalController.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);

      }
      if (this.rout.url == '/main/tab2/order-auction' || this.rout.url == '/main/tab2/order-wait' || this.rout.url == '/main/tab2/order-wait-send' || this.rout.url == '/main/tab2/order-send' || this.rout.url == '/main/tab2/order-cancel' || this.rout.url == '/main/tab3' || this.rout.url == '/main/notification/auction' || this.rout.url == '/main/notification/noti-system' || this.rout.url == '/main/account') {
        this.rout.navigate(['/main/home']);
      }
      else if (this.rout.url == '/main/cal-concrete') {
        this.rout.navigate(['/main/step2-1']);
      }
      else if (this.rout.url == '/auth/signup' || this.rout.url == '/auth/forgot-password') {
        this.rout.navigate(['/auth/login']);
      }
      else if (this.rout.url == '/main/home' || this.rout.url == '/auth/login') {
        if (new Date().getTime() - this.lastTimeBackPress >= this.timePeriodToExit) {
          this.lastTimeBackPress = new Date().getTime();
          this.Toast('กด Back อีกครั้ง เพื่อออกจาก App')
        } else {
          navigator["app"].exitApp();
        }
      }
    })
  }
  async Toast(text) {
    const toast = await this.toast.create({
      message: text,
      mode: 'ios',
      position: 'bottom',
      color: "dark",
      duration: 2000
    });
    toast.present();
  }
  async exit() {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      mode: 'ios',
      message: 'คุณต้องการการออกจากแอพหรือไม่ ?',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // some code
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            navigator["app"].exitApp();
          }
        }
      ]
    });

    await alert.present();
  }
  ngOnInit() {


  }
  async checkData() {
    await this.storage.get('walkthrough').then(async (res: any) => {
      if (!res) {
        await this.rout.navigate(['walkthrough']);
      } 
    })
    setTimeout(() => {
      this.splashScreen.hide();
    }, 1000);

  }
  async initializeApp() {
    console.log(this.network);
    // เช็คว่ามีการเชื่อมต่อ network หรือยัง
    if (this.network.type !== "none") {
      /*
      *---------------------------------------------------------------
      *  PUSH NOTIFICATION WITH FIREBASE
      *---------------------------------------------------------------
      */
      // ลงทะเบียน  device เพื่อรับ Token

      this.fcm.subscribeToTopic('all');
      this.fcm.getToken().then(token => {
        if (!this.platform.is('desktop')) {
          this.userDeviceData.token = token;
          // Device info
          this.userDeviceData.uuid = this.device.uuid;
          this.userDeviceData.platform = this.device.platform;
          this.userDeviceData.version = this.device.version;
          console.log(this.userDeviceData);
        } else {
          this.userDeviceData.token = '';
          // Device info
          this.userDeviceData.uuid = 'browser';
          this.userDeviceData.platform = 'web';
          this.userDeviceData.version = '1';
        }
        this.storage.get("data_login").then((data) => {
          if (data) {
            this.userDeviceData.member_id = data.member_id;
            this.webService.postData('register_device', this.userDeviceData).then((result) => {
              this.userDeviceResponse = result;
              if (this.userDeviceResponse.Sucess) {
                this.webService.Toast('Regis new device success');
                // Keep UUID to localStorage
                this.storage.set('uuid', this.device.uuid);
              } else {
                this.webService.Toast('Fail! Cannot Regis new device');
              }
            }, (error) => {
              //console.log(error);
              if (error.status == 0) {
                this.webService.Toast('Error status Code:0 Web API Offline');
              }
            });
          } else {
            this.userDeviceData.member_id = '0';
            this.webService.postData('register_device', this.userDeviceData).then((result) => {
              this.userDeviceResponse = result;
              if (this.userDeviceResponse.Sucess) {
                this.webService.Toast('Regis new device success');
                // Keep UUID to localStorage
                this.storage.set('uuid', this.device.uuid);
              } else {
                this.webService.Toast('Fail! Cannot Regis new device');
              }
            }, (error) => {
              //console.log(error);
              if (error.status == 0) {
                this.webService.Toast('Error status Code:0 Web API Offline');
              }
            });
          }
        });
        // Connect Web API


      }); // then

      // อัพเดท Token
      this.fcm.onTokenRefresh().subscribe(token => {
        this.userDeviceData.token = token;
        // Device info
        this.userDeviceData.uuid = this.device.uuid;
        this.userDeviceData.platform = this.device.platform;
        this.userDeviceData.version = this.device.version;

        // Connect Web API
        this.webService.postData('register_device', this.userDeviceData).then((result) => {
          this.userDeviceResponse = result;
          if (this.userDeviceResponse.Sucess) {
            // this.webService.Toast('Regis new device success');
            console.log('Regis new device success');

            // Keep UUID to localStorage
            this.storage.set('uuid', this.device.uuid);

          } else {
            // this.webService.Toast('Fail! Cannot Regis new device');
            console.log('Fail! Cannot Regis new device');
          }
        }, (error) => {
          //console.log(error);
          if (error.status == 0) {
            // this.webService.Toast('Error status Code:0 Web API Offline');
            console.log('Error status Code:0 Web API Offline');
          }
        });
      });


    } else if (this.network.type === 'none') {

      this.webService.presentAlert('error', 'not_internet');


    }

  }
  setLanguage() {
    this.webService.storage_get('lang').then((res: any) => {
      console.log(res);

      if (!res) {
        this.translate.setDefaultLang('th');
        this.webService.storage_set('lang', 'th')
      } else {
        if (res == 'th') {
          this.translate.setDefaultLang('th');
        } else if (res == 'en') {
          this.translate.setDefaultLang('en');
        } else {
          this.translate.setDefaultLang('th');
        }
      }
    })
    // this language will be used as a fallback when a translation isn't found in the current language
    // this.translate.setDefaultLang('en');
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    // this.translate.use('th');
    // this is to determine the text direction depending on the selected language
    // for the purpose of this example we determine that only arabic and hebrew are RTL.
    // this.translate.onDefaultLangChange.subscribe((event: LangChangeEvent) => {
    //   // this.textDir = (event.lang === 'th' || event.lang === 'en') ? 'th' : 'en';
    //   // console.log('13213213');
    //   // this.translate.use('en');
    // });
  }
  async fcmNotify() {
    this.fcm.onNotification().subscribe(async (res: any) => {
      console.log(res);
      if (res.wasTapped) {
        try {
          const element = await this.modalController.getTop();
          if (element) {
            element.dismiss();
          }
        } catch (error) {
          console.log(error);
        }
        console.log('Tapped');
        if (res.click_action == 'chat') {
          console.log('chat');
          this.rout.navigate(['/main/tab3']);
        } else if (res.click_action == 'auction') {
          console.log('auction');
          this.rout.navigate(['/main/notification/auction']);
          setTimeout(async () => {
            let data = {
              order_id: res.order_id
            }
            const modal = await this.modalController.create({
              component: AuctionCongretePage,
              componentProps: data
            });
            return await modal.present();
          }, 1000);
        } else if (res.click_action == 'accecpt') {
          console.log('accecpt');
          this.rout.navigate(['/main/notification/auction']);
          setTimeout(async () => {
            let data = {
              order_id: res.order_id
            }
            const modal = await this.modalController.create({
              component: AuctionCongretePage,
              componentProps: data
            });
            return await modal.present();
          }, 1000);
        }
      }
      console.log('NoTap');
    });
  }
  ionViewWillLeave() {
    this.subscribe.unsubscribe();
  }

}
