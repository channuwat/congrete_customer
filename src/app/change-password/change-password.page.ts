import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PasswordValidator } from '../validators/password.validator';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  matching_passwords_group: FormGroup;

  loginForm: FormGroup;
  public Phonerequired = 'Phone is required.';
  public Namerequired = 'Name is required.';
  public Emailrequired = 'Email is required.';
  public Addressrequired = 'Address is required.';
  public EmailPattern = 'Incorrect email pattern.';
  public EnterPhone = 'Enter a valid Phone.';
  public PhoneLeast = 'Phone must be at least 9 characters long.';
  public PhoneLargest = 'Phone must be at largest  10 characters long.';
  public Passwordrequired = 'Password is required.';
  public ConfirmPasswordrequired = 'Confirm Password is required.';
  public PasswordLeast = 'Password must be at least 6 characters long.';
  validation_messages = {
    password: [
      { type: 'required', message: this.Passwordrequired },
      { type: 'minlength', message: this.PasswordLeast }
    ],
    confirm_password: [
      { type: 'required', message: this.ConfirmPasswordrequired },

    ],
    name: [
      { type: 'required', message: this.Namerequired }
    ],
    email: [
      { type: 'required', message: this.Emailrequired },
      { type: 'pattern', message: this.EmailPattern }
    ],
    address: [
      { type: 'required', message: this.Addressrequired }
    ]
  };
  constructor(
    public router: Router,
    public api: WebapiServiceProvider,
    public translate: TranslateService
  ) {
    this.loginForm = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      confirm_password: new FormControl('', Validators.compose([Validators.required])),
      name: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      address: new FormControl('')
    });
  }
  ionViewWillEnter() {
    this.tranSub()
  }
  tranSub() {
    this.translate.get('Passwordrequired').subscribe(
      value => {
        this.Passwordrequired = value;
      }
    )
    this.translate.get('PasswordLeast').subscribe(
      value => {
        this.PasswordLeast = value;
      }
    )
    this.translate.get('ConfirmPasswordrequired').subscribe(
      value => {
        this.ConfirmPasswordrequired = value;
      }
    )
    this.translate.get('Namerequired').subscribe(
      value => {
        this.Namerequired = value;
      }
    )
    this.translate.get('Emailrequired').subscribe(
      value => {
        this.Emailrequired = value;
      }
    )
    this.translate.get('EmailPattern').subscribe(
      value => {
        this.EmailPattern = value;
      }
    )
    this.translate.get('Addressrequired').subscribe(
      value => {
        this.Addressrequired = value;
      }
    )
    this.validation_messages = {
      password: [
        { type: 'required', message: this.Passwordrequired },
        { type: 'minlength', message: this.PasswordLeast }
      ],
      confirm_password: [
        { type: 'required', message: this.ConfirmPasswordrequired },
  
      ],
      name: [
        { type: 'required', message: this.Namerequired }
      ],
      email: [
        { type: 'required', message: this.Emailrequired },
        { type: 'pattern', message: this.EmailPattern }
      ],
      address: [
        { type: 'required', message: this.Addressrequired }
      ]
    };

  }
  ngOnInit() {
  }
  doLogin() {
    console.log(this.loginForm.value);
    this.api.storage_get('otp_phone').then((val: any) => {
      let data = this.loginForm.value;
      data.phone = val;
      this.api.postData("add_password", data).then((result: any) => {
        console.log(result);
        if (result.flag == '1') {

          this.api.Toast("บันทึกข้อมูลเรียบร้อยแล้ว กรุณาเข้าสู่ระบบ");
          setTimeout(() => {
            this.router.navigate(['auth/login']);
          }, 1000);
        } else {
          this.api.Toast("รหัสผ่านไม่ตรงกัน");
        }
      });
    });

  }

}
