import { Component, ChangeDetectorRef } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(
    public firebase: AngularFireDatabase,
    public storage: Storage,
    public translate: TranslateService,
    public rout: Router,
    private ref: ChangeDetectorRef
  ) { }
  public newChat: any;
  public newAlert: any;
  public login_data = {
    member_id: ''
  }
  ngOnInit(): void {
    // setInterval(() => {
    //   this.ref.detectChanges()
    // }, 1000);

    this.storage.get('data_login').then((login: any) => {
      if (login) {
        this.login_data = login;
        console.log(login);
        this.firebase.database.ref("notification/user_" + this.login_data.member_id + '/chat').on("value", (val: any) => {
          this.newChat = val.node_.value_;
          this.ref.detectChanges();
        })
        this.firebase.database.ref("notification/user_" + login.member_id + '/alert').on("value", (val: any) => {
          this.newAlert = val.node_.value_;
          this.ref.detectChanges();
        })
      } else {
        console.log('No user login.');
        
      }



    });
  }
  setNotify() {
    this.storage.get('data_login').then((res: any) => {
      this.firebase.database.ref('notification/user_' + res.member_id + '/alert').set(0)
      this.ref.detectChanges()
    })
  }
  ionViewWillEnter() {
    this.storage.get('data_login').then((res: any) => {
      if (!res) {
        this.rout.navigate(['auth/login']);
      }
    })
  }
  
}
