import { WebapiServiceProvider } from './../providers/webapi-service/webapi-service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { IonContent, IonInfiniteScroll, IonInput } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']

})
export class Tab3Page implements OnInit {
  public chatForm: FormGroup = null;
  public chats = [];
  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, { static: false }) content: IonContent;
  @ViewChild('input', { static: false }) input: IonInput;
  constructor(public api: WebapiServiceProvider,
    public storage: Storage,
    private ref: ChangeDetectorRef,
    public translate: TranslateService,
    public firebase: AngularFireDatabase,

  ) {

    this.chatForm = new FormGroup({
      message: new FormControl('', Validators.compose([Validators.required]))
    })
  }
  ionViewDidEnter() {
    setTimeout(() => {
      this.scrollToBottom()
    }, 500);
  }
  public infinitscroll = false;
  scrollToBottom(): void {
    this.content.scrollToBottom();
    setTimeout(() => {
      this.infinitscroll = true;
    }, 500);
  }
  moreChat(event:any) {
    if (this.infinitscroll) {
      let page = Math.ceil(this.chats.length / 15) + 1;
      console.log(event);
      console.log(page);
      this.api.getData("load_chat/" + this.userOnline.member_id + "/" + page).then((res: any) => {
        console.log(res);
        res.slice().reverse().forEach(element => {
          this.chats.splice(0, 0, element)
        });
        setTimeout(() => {
          event.target.complete();
        }, 500);
      })
    }else{
      setTimeout(() => {
        event.target.complete();
      }, 500);
    }


  }
  public userOnline: any = {
    member_id: 0
  }
  ngOnInit(): void {
    this.storage.get('data_login').then((login: any) => {
      this.userOnline = login;
      this.firebase.database.ref("chat/" + login.member_id).on("value", (val) => {
        console.log(login.member_id);
        this.api.getData("load_chat/" + login.member_id + "/1").then((res: any) => {
          this.chats = res;
          console.log(res);
          setTimeout(() => {
            this.ref.detectChanges();
            this.scrollToBottom();
          }, 300);
        })
      })
    });
  }
  send() {
    setTimeout(() => {
      this.input.setFocus()
    }, 100);
    this.sending = true;
    let data = this.chatForm.value;
    if (data.message.trim() != '') {
      this.storage.get('data_login').then((login: any) => {
        data.member_id = login.member_id;
        this.api.postData("add_chat", { data, img: '' }).then((res: any) => {
          console.log(res);
          let a = this.firebase.database.ref('chat/' + data.member_id).set(Math.round(Math.random() * 100));
          this.scrollToBottom();
          this.sending = false;
        });

        this.chatForm.setValue({ message: '' });
      });
    } else {
      this.sending = false;
    }



  }
  public imagePath: any = '';
  public url = '';
  public reader: any;
  public type: any;
  public formData = new FormData();
  public sending = false;
  changeImg(event): void {
    this.sending = true;
    let data = {
      member_id: '',
      message: ''
    };
    console.log(event);
    this.formData.append('avatar', event.target.files[0]);
    this.reader = new FileReader();
    this.reader.onload = (event: any) => {
      this.url = event.target.result;
    }
    this.reader.readAsDataURL(event.target.files[0]);
    this.type = event.target.files[0].type;
    console.log(this.reader.result);

    setTimeout(() => {
      this.storage.get('data_login').then((login: any) => {
        data.member_id = login.member_id;
        this.api.postData("add_chat", { data, img: this.reader.result, type: this.type }).then((res: any) => {
          console.log(res);
          let a = this.firebase.database.ref('chat/' + data.member_id).set(Math.round(Math.random() * 100));
          this.scrollToBottom();
          this.sending = false;
        });
      });
    }, 1000);
  }
  read() {
    this.firebase.database.ref("notification/user_" + this.userOnline.member_id + '/chat').set(0);
    setTimeout(() => {
      this.scrollToBottom();
    }, 100);
  }
  saveImg(url) {

  }
}
