import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { Storage } from '@ionic/storage';
import { AlertController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-config-account',
  templateUrl: './config-account.page.html',
  styleUrls: ['./config-account.page.scss'],
})
export class ConfigAccountPage implements OnInit {
  @Input() member_id=''
  matching_passwords_group: FormGroup;

  loginForm: FormGroup;

  validation_messages = {
    name: [
      { type: 'required', message: 'Address is required.' }
    ],
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'รูปแบบไม่ถูกต้อง' }
    ],
    address: [
      { type: 'required', message: 'Address is required.' }
    ]
  };

  public member: any = 0
  constructor(
    public route: Router,
    public router: ActivatedRoute,
    public store: Storage,
    public service: WebapiServiceProvider,
    public alert: AlertController,
    public translate: TranslateService,
    public modal: ModalController,
    private ref: ChangeDetectorRef
  ) {
    // this.member = this.router.snapshot.paramMap.get('member_id')
    this.loginForm = new FormGroup({
      name: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      address: new FormControl('', Validators.compose([]))
    });
  }

  ngOnInit() {
    console.log(this.loginForm);

  }

  ionViewWillEnter() {
    this.loadData()
  }

  public acc: any = {}
  loadData() {
    this.service.getData('getDataAccount/' + this.member_id).then((dataAcc: any) => {
      this.loginForm.patchValue({
        name: dataAcc.name,
        email: dataAcc.email,
        address: dataAcc.address
      })
      this.url = dataAcc.picture
      this.acc.member_id = dataAcc.member_id
      this.store.set('data_login', dataAcc)
    })
  }

  public imagePath: any = '';
  public url = '';
  async uploadProfile(e) {
    // e.preventDefault();
    // console.log(e);
    let el: HTMLElement = document.getElementById('inputFile') as HTMLElement;
    el.click();
  }
  public formData = new FormData();
  public reader: any
  public typeIMG: any
  changeListener(event): void {

    this.formData.append('avatar', event.target.files[0]);
    this.reader = new FileReader();
    this.reader.onload = (event: any) => {
      this.url = event.target.result;
    }
    this.reader.readAsDataURL(event.target.files[0]);
    this.typeIMG = event.target.files[0].type

  }

  async save() {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      mode:'ios',
      message: 'ยืนยันการเเก้ไข',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // some code
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            var img: any
            var type: ''
            this.acc.name = this.loginForm.value.name,
              this.acc.email = this.loginForm.value.email,
              this.acc.address = this.loginForm.value.address
            if (!this.reader) {
              img = null
              console.log('null');
              this.service.postData('configAccount', { input: this.acc, img: null, type: null }).then(() => {
                this.service.Toast('แก้ไขข้อมูลสำเร็จ')
                this.loadData()
                setTimeout(() => {
                  this.close()
                }, 1000);
              })

            } else {
              img = this.reader.result
              if (this.typeIMG != '') {
                type = this.typeIMG
              }
              this.service.postData('configAccount', { input: this.acc, img: img, type: type }).then(() => {
                this.service.Toast('แก้ไขข้อมูลสำเร็จ')
                this.loadData()
                setTimeout(() => {
                  this.close()
                }, 1000);
              })

            }

          }
        }
      ]
    });

    await alert.present();
  }

  close() {
    this.modal.dismiss()
    // this.route.navigate(['../main/account'])
  }

}
