import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConfigAccountPage } from './config-account.page';

describe('ConfigAccountPage', () => {
  let component: ConfigAccountPage;
  let fixture: ComponentFixture<ConfigAccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigAccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConfigAccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
