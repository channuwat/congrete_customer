import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { ModalController } from '@ionic/angular';
import { AuctionCongretePage } from '../auction-congrete/auction-congrete.page';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-order-success',
  templateUrl: './order-success.page.html',
  styleUrls: ['./order-success.page.scss'],
})
export class OrderSuccessPage implements OnInit {
  public last_order: any;
  public sub: any;
  public order = {
    order_id: 0,
    end_date: '2020-01-01 10:00:00'
  }
  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public api: WebapiServiceProvider,
    public translate: TranslateService,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    // this.route.queryParams.subscribe(params => {
    //   if (params) {
    //     this.last_order = params;
    //   }
    // });
    this.last_order = this.route.snapshot.paramMap.get('order');
  }
  ionViewWillEnter() {
    this.api.storage_get('data_login').then((data: any) => {
      this.api.getData('order_data/' + data.member_id + '/10').then((res: any) => {
        this.order = res[0];
        console.log(this.order);
      });
    });
  }

  next() {
    console.log(this.last_order);
    console.log(this.order);


  }
  async showdetails() {
    this.router.navigate(['/main/home/']);
    const modal = await this.modalController.create({
      component: AuctionCongretePage,
      componentProps: this.order
    });
    return await modal.present();
  }
}
