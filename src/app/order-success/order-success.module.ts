import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderSuccessPageRoutingModule } from './order-success-routing.module';

import { OrderSuccessPage } from './order-success.page';
import { AuctionCongretePage } from '../auction-congrete/auction-congrete.page';
import { AuctionCongretePageModule } from '../auction-congrete/auction-congrete.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderSuccessPageRoutingModule,
    AuctionCongretePageModule,
    TranslateModule.forChild()
  ],
  declarations: [OrderSuccessPage],
  entryComponents:[AuctionCongretePage]
})
export class OrderSuccessPageModule {}
