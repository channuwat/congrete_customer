import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalConcretePage } from './cal-concrete.page';

const routes: Routes = [
  {
    path: '',
    component: CalConcretePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalConcretePageRoutingModule {}
