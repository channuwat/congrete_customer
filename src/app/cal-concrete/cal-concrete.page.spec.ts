import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CalConcretePage } from './cal-concrete.page';

describe('CalConcretePage', () => {
  let component: CalConcretePage;
  let fixture: ComponentFixture<CalConcretePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalConcretePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CalConcretePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
