import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-cal-concrete',
  templateUrl: './cal-concrete.page.html',
  styleUrls: ['./cal-concrete.page.scss'],
})
export class CalConcretePage implements OnInit {

  public formGroupCalc: FormGroup
  constructor(
    public translate: TranslateService,
    public router: Router
  ) { }

  ngOnInit() {
    this.formGroupCalc = new FormGroup({
      input_1: new FormControl(0),
      input_2: new FormControl(0),
      input_3: new FormControl(0),
      input_4: new FormControl(1),
    })
  }

  public type_work = [
    { value: 1, css: 'active' },
    { value: 2, css: 'none' },
    { value: 3, css: 'none' },
    { value: 4, css: 'none' },
  ]
  public img_part = 'work1.png'
  selectTypeWork(type) {
    this.type_work.forEach(menu => {
      if (menu.value == type) {
        menu.css = 'active'
        this.img_part = 'work' + type + '.png'
      } else {
        menu.css = 'none'
      }
    });
  }

  public result_clac: number = 0
  public result_com: number = 0

  calcCongrete() {
    let inp_1 = this.formGroupCalc.value.input_1
    let inp_2 = this.formGroupCalc.value.input_2
    let inp_3 = this.formGroupCalc.value.input_3
    let inp_4 = this.formGroupCalc.value.input_4
    this.result_clac = (inp_1*inp_2)*(inp_3*inp_4)
    this.result_com = Math.ceil(this.result_clac*4)/ 4;
    console.log(this.result_clac);
    console.log(this.result_com);
    
  }

  back() {
    this.router.navigate(['/main/step2-1']);
  }

}
