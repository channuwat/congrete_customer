import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalConcretePageRoutingModule } from './cal-concrete-routing.module';

import { CalConcretePage } from './cal-concrete.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CalConcretePageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [CalConcretePage]
})
export class CalConcretePageModule {}
