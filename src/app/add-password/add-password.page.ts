import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PasswordValidator } from '../validators/password.validator';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { TranslateService } from '@ngx-translate/core';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-add-password',
  templateUrl: './add-password.page.html',
  styleUrls: ['./add-password.page.scss'],
})
export class AddPasswordPage implements OnInit {

  matching_passwords_group: FormGroup;

  loginForm: FormGroup;
  loginForm2: FormGroup;

  validation_messages = {
    password: [
      { type: 'required', message: 'Password is required.' },

    ],
    confirm_password: [
      { type: 'required', message: 'Confirm Password is required.' },
      // { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };
  constructor(
    public router: Router,
    public api: WebapiServiceProvider,
    public modal: ModalController,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public translate: TranslateService
  ) {
    this.loginForm = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areNotEqual(formGroup);
    });
    this.loginForm2 = new FormGroup({
      oldpassword: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areNotEqual(formGroup);
    });
  }
  change() {
    console.log(this.loginForm.invalid.valueOf);
  }

  ngOnInit() {
  }
  public pwd = 0;
  public data: any;
  ionViewWillEnter() {
    this.oldpwdCheck = false;
    this.api.storage_get('data_login').then((data: any) => {
      this.data = data;
      this.api.postData('checkPassword', { member_id: data.member_id }).then((res: any) => {
        console.log(res);
        this.pwd = res;
      })
    });


  }
  private oldpwdCheck = false
  checkFocus() {
    console.log(this.loginForm.value.oldpassword);
    this.api.postData('matchPwd', { member_id: this.data.member_id, pwd: this.loginForm.value.oldpassword }).then((res: any) => {
      this.oldpwdCheck = res;
    })
  }
  close() {
    console.log('close');
    this.modal.dismiss()
  }
  doLogin() {
    console.log(this.loginForm.value);
    this.api.storage_get('otp_phone').then((val: any) => {
      let data = this.loginForm.value;
      data.phone = val;
      this.api.postData("add_password", data).then((result: any) => {
        console.log(result);
        if (result.flag == '1') {
          this.api.Toast("บันทึกข้อมูลเรียบร้อยแล้ว");
          setTimeout(() => {
            this.router.navigate(['auth/login']);
          }, 1000);
        } else {
          this.api.Toast("รหัสผ่านไม่ตรงกัน");
        }
      });
    });

  }
  async presentSaveNew() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'กำลังตรวจสอบ...'

    });
    await loading.present();
    let data = this.loginForm.value;
    data.oldpassword = '';
    this.api.postData('newPassword', { data: data, member_id: this.data.member_id }).then(async (res: any) => {
      if (res == 'Success') {
        await loading.dismiss();
        this.presentAlert()
      } else {
        await loading.dismiss();
        this.errorAlert()
      }

    })
  }

  async presentSave() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'กำลังตรวจสอบ...'

    });
    await loading.present();
    this.api.postData('newPassword', { data: this.loginForm2.value, member_id: this.data.member_id }).then(async (res: any) => {
      if (res == 'Success') {
        await loading.dismiss();
        this.presentAlert()
      } else {
        await loading.dismiss();
        this.errorAlert()
      }

    })
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      mode: 'ios',
      header: 'สำเร็จ',
      message: 'บันทึกข้อมูลเรียบร้อย',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('OK');
            this.close()
          }
        },
      ]
    });

    await alert.present();

  }
  async errorAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      mode: 'ios',
      message: 'รหัสผ่านเดิมไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            console.log('OK');
          }
        },
      ]
    });

    await alert.present();

  }
}
