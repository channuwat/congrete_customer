import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { Router, ActivatedRoute } from '@angular/router';
import { IonRefresher, ModalController } from '@ionic/angular';
import { AuctionCongretePage } from 'src/app/auction-congrete/auction-congrete.page';
import { TranslateService } from '@ngx-translate/core';
import { PaymentPage } from 'src/app/payment/payment.page';

@Component({
  selector: 'app-order-wait',
  templateUrl: './order-wait.page.html',
  styleUrls: ['./order-wait.page.scss'],
})
export class OrderWaitPage implements OnInit {
  @ViewChild(IonRefresher, { static: false }) refresher: IonRefresher;
  constructor(
    public router: Router,
    public api: WebapiServiceProvider,
    public translate: TranslateService,
    public ModalController: ModalController,
    public ref: ChangeDetectorRef
  ) { }
  public ordernum = 0;
  public orders = [];
  public member = {
    member_id: ''
  }
  public fbstatus = false;
  ngOnInit() {
    this.fbstatus = true
    this.api.storage_get('data_login').then((data: any) => {
      this.member = data
      console.log(this.member.member_id);

      this.api.getData('order_data/' + this.member.member_id + '/0/1').then((res: any) => {
        this.orders = res;

      })
    });
    this.api.fb_val('order', (res: any) => {
      console.log(res);

      if (this.fbstatus) {
        this.load_order();
      }
    })
  }
  swipeEvent(e) {
    console.log(e);

  }
  moreOrder(event) {
    let page = Math.ceil(this.orders.length / 10) + 1;
    this.api.getData('order_data/' + this.member.member_id + '/0/' + page).then((res: any) => {
      res.forEach(element => {
        this.orders.push(element)
      });
      setTimeout(() => {
        event.target.complete();
      }, 500);
    })
  }
  load_order() {
    this.api.getData('order_data/' + this.member.member_id + '/0/1').then((res: any) => {
      this.orders = res;
      this.ordernum = res.length;
      console.log(this.orders);
      console.log(this.ordernum);
      this.ref.detectChanges();
    });
  }

  // showdetails(data){
  //   console.log(data);
  //   this.router.navigate(['/main/open-auction/' + data.order_id]);
  // }

  async showdetails(data) {
    if (data.status == 0) {
      const modal = await this.ModalController.create({
        component: AuctionCongretePage,
        componentProps: data
      });
      return await modal.present();
    } else {
      let order = data.order_id;
      const modal = await this.ModalController.create({
        component: PaymentPage,
        componentProps: { order: order }
      });
      return await modal.present();
    }
  }
  async doRefresh(event) {
    await this.ngOnInit();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
    this.ngOnInit();
  }
  ionViewWillLeave() {
    this.refresher.disabled = true;
    this.fbstatus = false;
  }

}
