import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderWaitPageRoutingModule } from './order-wait-routing.module';

import { OrderWaitPage } from './order-wait.page';
import { AuctionCongretePage } from 'src/app/auction-congrete/auction-congrete.page';
import { AuctionCongretePageModule } from 'src/app/auction-congrete/auction-congrete.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderWaitPageRoutingModule,
    TranslateModule.forChild(),
    AuctionCongretePageModule
  ],
  declarations: [OrderWaitPage],
  entryComponents:[AuctionCongretePage]
})
export class OrderWaitPageModule {}
