import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { AuctionCongretePage } from 'src/app/auction-congrete/auction-congrete.page';
import { ModalController, AlertController, IonRefresher } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-order-send',
  templateUrl: './order-send.page.html',
  styleUrls: ['./order-send.page.scss'],
})
export class OrderSendPage implements OnInit {

  @ViewChild(IonRefresher, { static: false }) refresher: IonRefresher;
  constructor(public api: WebapiServiceProvider,
    public ModalController: ModalController,
    public translate: TranslateService,
    public alertController: AlertController,
    public ref: ChangeDetectorRef) { }
  public orders = [];
  public fbstatus = false;
  public member: any;
  ngOnInit() {
    this.fbstatus = true
    this.api.storage_get('data_login').then((data: any) => {
      this.member = data
      this.api.getData('order_data/' + this.member.member_id + '/3').then((res: any) => {
        this.orders = res;
      })
      this.api.fb_val('order', (res: any) => {
        console.log(res);

        if (this.fbstatus) {
          this.load_order();
        }
      })
    });
  }
  public ordernum = 0;
  load_order() {
    this.api.getData('order_data/' + this.member.member_id + '/3/1').then((res: any) => {
      console.log('old : ' + this.orders.length);
      console.log('new : ' + res.length);
      if (this.orders.length != res.length) {
        this.orders = res;
        this.ordernum = res.length;

        this.ref.detectChanges();
      }

    });
  }
  // remove(order_id) {

  //   console.log(order_id);
  //   this.api.getData('hidden_order/' + order_id).then((res: any) => {
  //     this.load_order();
  //     this.ref.detectChanges();

  //   })
  // }
  async remove(order_id) {
    const alert = await this.alertController.create({
      cssClass: 'alert',
      mode: 'ios',
      header: 'ลบรายการ',
      message: 'คุณต้องการลบรายการนี้ ใช่หรือไม่',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            this.api.getData('hidden_order/' + order_id).then((res: any) => {
              this.load_order();
              this.ref.detectChanges();

            })
          }
        }
      ]
    });

    await alert.present();
  }
  moreOrder(event) {
    let page = Math.ceil(this.orders.length / 10) + 1;
    this.api.getData('order_data/' + this.member.member_id + '/3/' + page).then((res: any) => {
      res.forEach(element => {
        this.orders.push(element)
      });
      setTimeout(() => {
        event.target.complete();
      }, 500);
    })
  }
  async showdetails(data) {
    const modal = await this.ModalController.create({
      component: AuctionCongretePage,
      componentProps: data
    });
    return await modal.present();
  }
  async doRefresh(event) {
    await this.ngOnInit();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
    this.ngOnInit();
  }
  ionViewWillLeave() {
    this.refresher.disabled = true;
    this.fbstatus = false;
  }
}
