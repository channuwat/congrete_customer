import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderSendPageRoutingModule } from './order-send-routing.module';

import { OrderSendPage } from './order-send.page';
import { AuctionCongretePage } from 'src/app/auction-congrete/auction-congrete.page';
import { AuctionCongretePageModule } from 'src/app/auction-congrete/auction-congrete.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderSendPageRoutingModule,
    TranslateModule.forChild(),
    AuctionCongretePageModule
  ],
  declarations: [OrderSendPage],
  entryComponents:[AuctionCongretePage]
})
export class OrderSendPageModule {}
