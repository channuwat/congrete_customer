import { Component, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  public selected = 0;
  constructor(
    public router: Router,
    public translate: TranslateService,
    public api: WebapiServiceProvider,
    public ref: ChangeDetectorRef
  ) { }
  public active = false;
  public member = {
    member_id: 0
  }
  public waitSend = 0;
  public waitPay = 0;
  public auction = 0;
  page(uri) {
    this.router.navigate(['/main/tab2/' + uri]);
    if (uri == 'order-auction') {
      this.selected = 10;
    } else if (uri == 'order-wait') {
      this.selected = 0;
    } else if (uri == 'order-wait-send') {
      this.selected = 1;
    } else if (uri == 'order-send') {
      this.selected = 2;
    } else if (uri == 'order-cancel') {
      this.selected = -1;
    }
  }
  ngOnInit() {
    this.active = true;
    this.api.storage_get('data_login').then((res) => {
      this.member = res;
    })
    this.api.fb_val('order', (res: any) => {
      if (this.active) {
        this.orderCount(this.member.member_id);
      }
    })
  }
  orderCount(member_id) {
    this.api.getData('count_order/' + member_id).then((res: any) => {
      this.waitSend = res.waitSend;
      this.waitPay = res.waitPay;
      this.auction = res.auction;
      this.ref.detectChanges();
    })
  }
  ionViewWillEnter() {
    this.active = true
    setTimeout(() => {
      this.orderCount(this.member.member_id);
    }, 500);
    this.selected = 10;
  }
  ngOnDestroy() {
    this.active = false
  }

}
