import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderWaitSendPageRoutingModule } from './order-wait-send-routing.module';

import { OrderWaitSendPage } from './order-wait-send.page';
import { AuctionCongretePage } from 'src/app/auction-congrete/auction-congrete.page';
import { AuctionCongretePageModule } from 'src/app/auction-congrete/auction-congrete.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderWaitSendPageRoutingModule,
    TranslateModule.forChild(),
    AuctionCongretePageModule
  ],
  declarations: [OrderWaitSendPage],
  entryComponents:[AuctionCongretePage]
})
export class OrderWaitSendPageModule {}
