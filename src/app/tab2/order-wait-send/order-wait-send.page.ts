import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { IonRefresher, ModalController } from '@ionic/angular';
import { AuctionCongretePage } from 'src/app/auction-congrete/auction-congrete.page';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-order-wait-send',
  templateUrl: './order-wait-send.page.html',
  styleUrls: ['./order-wait-send.page.scss'],
})
export class OrderWaitSendPage implements OnInit {
  @ViewChild(IonRefresher, { static: false }) refresher: IonRefresher;
  constructor(public api: WebapiServiceProvider,
    public ModalController: ModalController,
    public translate: TranslateService,
    public ref: ChangeDetectorRef) { }
  public orders = [];
  public fbstatus = false;
  public member: any;
  ngOnInit() {
    this.fbstatus = true
    this.api.storage_get('data_login').then((data: any) => {
      this.member = data
      this.api.getData('order_data/' + this.member.member_id + '/1/1').then((res: any) => {
        this.orders = res;
      })
      this.api.fb_val('order', (res: any) => {
        console.log(res);

        if (this.fbstatus) {
          this.load_order();
        }
      })
    });
  }
  moreOrder(event) {
    let page = Math.ceil(this.orders.length / 10) + 1;
    this.api.getData('order_data/' + this.member.member_id + '/1/' + page).then((res: any) => {
      res.forEach(element => {
        this.orders.push(element)
      });
      setTimeout(() => {
        event.target.complete();
      }, 500);
    })
  }
  public ordernum = 0;
  load_order() {
    this.api.getData('order_data/' + this.member.member_id + '/1/1').then((res: any) => {
      this.orders = res;
      this.ordernum = res.length;
      console.log(this.orders);
      this.ref.detectChanges();
    });
  }
  async showdetails(data) {
    console.log(data);

    const modal = await this.ModalController.create({
      component: AuctionCongretePage,
      componentProps: data
    });
    return await modal.present();
  }
  async doRefresh(event) {
    await this.ngOnInit();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
    this.ngOnInit();
  }

  ionViewWillLeave() {
    this.refresher.disabled = true;
    this.fbstatus = false;
  }

}
