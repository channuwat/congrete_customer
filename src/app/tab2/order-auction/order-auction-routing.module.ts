import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderAuctionPage } from './order-auction.page';

const routes: Routes = [
  {
    path: '',
    component: OrderAuctionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderAuctionPageRoutingModule {}
