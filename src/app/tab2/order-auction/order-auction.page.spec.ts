import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderAuctionPage } from './order-auction.page';

describe('OrderAuctionPage', () => {
  let component: OrderAuctionPage;
  let fixture: ComponentFixture<OrderAuctionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderAuctionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderAuctionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
