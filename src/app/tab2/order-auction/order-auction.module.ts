import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderAuctionPageRoutingModule } from './order-auction-routing.module';

import { OrderAuctionPage } from './order-auction.page';
import { TranslateModule } from '@ngx-translate/core';
import { AuctionCongretePageModule } from 'src/app/auction-congrete/auction-congrete.module';
import { AuctionCongretePage } from 'src/app/auction-congrete/auction-congrete.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderAuctionPageRoutingModule,
    TranslateModule.forChild(),
    AuctionCongretePageModule
  ],
  declarations: [OrderAuctionPage],
  entryComponents:[AuctionCongretePage]
})
export class OrderAuctionPageModule {}
