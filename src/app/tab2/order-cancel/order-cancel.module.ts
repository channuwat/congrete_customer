import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderCancelPageRoutingModule } from './order-cancel-routing.module';

import { OrderCancelPage } from './order-cancel.page';
import { AuctionCongretePage } from 'src/app/auction-congrete/auction-congrete.page';
import { AuctionCongretePageModule } from 'src/app/auction-congrete/auction-congrete.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderCancelPageRoutingModule,
    TranslateModule.forChild(),
    AuctionCongretePageModule
  ],
  declarations: [OrderCancelPage],
  entryComponents:[AuctionCongretePage]
})
export class OrderCancelPageModule {}
