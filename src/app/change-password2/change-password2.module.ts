import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChangePassword2PageRoutingModule } from './change-password2-routing.module';

import { ChangePassword2Page } from './change-password2.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ReactiveFormsModule,
    ChangePassword2PageRoutingModule
  ],
  declarations: [ChangePassword2Page]
})
export class ChangePassword2PageModule { }
