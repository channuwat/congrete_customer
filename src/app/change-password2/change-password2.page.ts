import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';

@Component({
  selector: 'app-change-password2',
  templateUrl: './change-password2.page.html',
  styleUrls: ['./change-password2.page.scss'],
})
export class ChangePassword2Page implements OnInit {


  matching_passwords_group: FormGroup;

  loginForm: FormGroup;

  validation_messages = {
    password: [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' }
    ],
    confirm_password: [
      { type: 'required', message: 'Confirm Password is required.' },

    ]
  };
  constructor(public router: Router, public api: WebapiServiceProvider) {
    this.loginForm = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      confirm_password: new FormControl('', Validators.compose([Validators.required]))
    });
  }

  ngOnInit() {
  }
  doLogin() {
    console.log(this.loginForm.value);
    this.api.storage_get('otp_phone').then((val: any) => {
      let data = this.loginForm.value;
      data.phone = val;
      this.api.postData("add_password", data).then((result: any) => {
        console.log(result);
        if (result.flag == '1') {
          this.api.Toast("บันทึกข้อมูลเรียบร้อยแล้ว");
          setTimeout(() => {
            this.router.navigate(['auth/login']);
          }, 1000);
        } else {
          this.api.Toast("รหัสผ่านไม่ตรงกัน");
        }
      });
    });

  }

}
