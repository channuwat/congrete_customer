import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmPasswordPageRoutingModule } from './confirm-password-routing.module';

import { ConfirmPasswordPage } from './confirm-password.page';
import { ComponentsModule } from '../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    ConfirmPasswordPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [ConfirmPasswordPage]
})
export class ConfirmPasswordPageModule {}
