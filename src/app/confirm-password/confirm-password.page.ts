import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from '@ionic/angular';
import { FCM } from '@ionic-native/fcm/ngx';
import { Device } from '@ionic-native/device/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-confirm-password',
  templateUrl: './confirm-password.page.html',
  styleUrls: ['./confirm-password.page.scss'],
})
export class ConfirmPasswordPage implements OnInit {
  signupForm: FormGroup;
  matching_passwords_group: FormGroup;


  validation_messages = {
    "otp1": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp2": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp3": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp4": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp5": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    "otp6": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],

  };
  public code = '';
  constructor(
    public router: Router,
    public api: WebapiServiceProvider,
    public translate: TranslateService,
    private platform: Platform,
    public device: Device,
    public fcm: FCM,
    private storage: Storage,
  ) {
    api.storage_get('otp_code').then((code) => {
      console.log(code);
      this.code = code;
    });
    this.signupForm = new FormGroup({
      otp1: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp2: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp3: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp4: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp5: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp6: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
    });
  }
  public userDeviceData = { uuid: "", token: "", platform: "", version: "", member_id: '0' };
  public userDeviceResponse: any;
  public fbuser: any;
  doSignup() {
    console.log(this.signupForm.value);
    this.api.storage_get('otp_phone').then((val: any) => {
      let otp = this.signupForm.value.otp1 + this.signupForm.value.otp2 + this.signupForm.value.otp3 + this.signupForm.value.otp4 + this.signupForm.value.otp5 + this.signupForm.value.otp6
      this.api.postData('confirm_otp2', { otp: otp, tel: val, fb: this.fromfacebook }).then((result: any) => {
        console.log(result);
        if (result.flag == '1') {
          this.router.navigate(['/auth/change-password']);
        } else if (result.flag == '0') {
          this.api.Toast("รหัสไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง หากยังไม่ได้รับให้กดส่งรหัสอีกครั้ง")
        } else if (result.flag == '2') {
          this.api.storage_get('fbuser').then((res: any) => {
            this.fbuser = res;
            this.api.postData('fblogin2', { id: res.id, email: res.email, name: res.name, picture: res.picture, tel: val }).then((res: any) => {
              if (res.flag == '1') {
                this.api.storage_set("data_login", res.data);
                if (!this.platform.is('desktop')) {
                  this.fcm.subscribeToTopic('all');
                  this.fcm.getToken().then(token => {

                    this.userDeviceData.token = token;
                    // Device info
                    this.userDeviceData.uuid = this.device.uuid;
                    this.userDeviceData.platform = this.device.platform;
                    this.userDeviceData.version = this.device.version;
                    console.log(this.userDeviceData);

                    this.userDeviceData.member_id = res.data.member_id;
                    this.api.postData('register_device', this.userDeviceData).then((result) => {
                      setTimeout(() => {
                        this.router.navigate(['main/home']);
                      }, 1000);
                      this.userDeviceResponse = result;
                      if (this.userDeviceResponse.Sucess) {
                        // this.api.Toast('Regis new device success');
                        // Keep UUID to localStorage
                        this.storage.set('uuid', this.device.uuid);
                      } else {
                        // this.api.Toast('Fail! Cannot Regis new device');
                      }
                    }, (error) => {
                      //console.log(error);
                      if (error.status == 0) {
                        // this.api.Toast('Error status Code:0 Web API Offline');
                      }
                    });
                    // Connect Web API
                  }); // then
                } else {
                  this.router.navigate(['main/home']);
                }
                setTimeout(() => {
                  this.router.navigate(['/main/home']);
                }, 1000);
              }
            })
          })
          // this.router.navigate(['/main/home']);
        }
      })
    });


  }

  public fromfacebook = ''
  ngOnInit() {
    this.api.storage_get('fbuser').then((res: any) => {
      this.fromfacebook = res;
    })
  }
  send_agen() {
    this.api.storage_get('otp_phone').then((val: any) => {
      this.api.postData('add_otp', { tel: val, type: 2 }).then((res: any) => {
        console.log(res);
        if (res.flag == '0') {
          this.api.Toast(res.error);
        } else {
          this.api.Toast("ระบบได้ส่ง SMS ไปยังเบอร์ของคุณเรียบร้อยแล้ว");

        }
      });
    });
  }
  otpController(event, next, prev) {
    if (event.target.value.length < 1 && prev) {
      prev.setFocus()
    }
    else if (next && event.target.value.length > 0) {
      next.setFocus();
    }
    else {
      return 0;
    }
  }
}
