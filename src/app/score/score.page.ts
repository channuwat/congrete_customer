import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { AlertController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-score',
  templateUrl: './score.page.html',
  styleUrls: ['./score.page.scss'],
})
export class ScorePage implements OnInit {
  @Input() member_id = ''

  constructor(
    public modal: ModalController,
    public route: Router,
    public router: ActivatedRoute,
    public store: Storage,
    public service: WebapiServiceProvider,
    public alert: AlertController,
    public translate: TranslateService,
    public location: Location
  ) { }


  ngOnInit() {
  }

  public point_total = 0
  public score_detail: any = []
  ionViewWillEnter() {
    this.service.getData('score_details/' + this.member_id + '/' + 1).then((data_score: any) => {
      console.log(data_score);

      this.score_detail = data_score.score
      this.point_total = data_score.total
    })
  }
  moreScore(event) {
    let page = Math.ceil(this.score_detail.length / 20) + 1
    console.log(page);
    this.service.getData('score_details/' + this.member_id + '/' + page).then((data_score: any) => {
      console.log(data_score);
      if (data_score.score != 0) {
        data_score.score.forEach(element => {
          this.score_detail.push(element)
        });
      }
      setTimeout(() => {
        event.target.complete();
      }, 500);
    })
  }
  close() {
    this.modal.dismiss()
  }
  async doRefresh(event) {
    await this.ionViewWillEnter();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }


}
