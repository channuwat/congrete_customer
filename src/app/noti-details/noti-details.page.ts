import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';

@Component({
  selector: 'app-noti-details',
  templateUrl: './noti-details.page.html',
  styleUrls: ['./noti-details.page.scss'],
})
export class NotiDetailsPage implements OnInit {
  @Input() data: any;
  constructor(
    public modalController: ModalController,
    public translate: TranslateService,
    public api: WebapiServiceProvider,
    public NavParams: NavParams,
  ) { }
  public noti_id = 0
  public lang = 'th'
  public body = {
    title:{th:'',en:''},
    content:{th:'',en:''}
  }
  ngOnInit() {

  }
  ionViewWillEnter() {
    this.noti_id = this.NavParams.get('noti_id')
    console.log(this.noti_id);
    this.api.storage_get('lang').then((res) => {
      this.lang = res;
    })
    this.api.getData('noti_details/' + this.noti_id + '/' + this.lang).then((res: any) => {
      console.log(res);
      this.body = res;
    })
  }
  close() {
    this.modalController.dismiss()
  }

}
