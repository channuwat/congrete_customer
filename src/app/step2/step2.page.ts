import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-step2',
  templateUrl: './step2.page.html',
  styleUrls: ['./step2.page.scss'],
})
export class Step2Page implements OnInit {

  public products = [];
  constructor(public modalController: ModalController, 
    public rout: Router, 
    public api: WebapiServiceProvider,
    public translate: TranslateService,
    ) {

  }
  doLogin() {
  }
  ngOnInit() {
    this.api.getData('load_products').then((res: any) => {
      console.log(res);
      this.products = res;
    })

  }
  select(type) {
    this.api.storage_set('step2', type);
    setTimeout(() => {
      this.rout.navigate(['/main/step2-1']);
    }, 100);

  }

  next() {
    this.rout.navigate(['/main/step2'])
  }
  back() {
    this.rout.navigate(['/main/step1'])
  }
  async doRefresh(event) {
    await this.ngOnInit();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }

}
