import { NotificationDetailPage } from './../../notification-detail/notification-detail.page';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { IonRefresher, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { NotiDetailsPage } from 'src/app/noti-details/noti-details.page';

@Component({
  selector: 'app-noti-system',
  templateUrl: './noti-system.page.html',
  styleUrls: ['./noti-system.page.scss'],
})
export class NotiSystemPage implements OnInit {
  @ViewChild(IonRefresher, { static: false }) refresher: IonRefresher;
  constructor(
    public service: WebapiServiceProvider,
    public fb: AngularFireDatabase,
    public translate: TranslateService,
    public ref: ChangeDetectorRef,
    public router: Router,
    public modalController: ModalController,
    private youtube: YoutubeVideoPlayer
  ) { }
  public user = {
    member_id: ''
  }
  public statePage = false
  public lang = 'th'
  page = 1;
  ngOnInit() {
    this.service.storage_get('data_login').then((data_u: any) => {
      this.user = data_u
      this.fb.database.ref('notification/user_' + this.user.member_id + '/alert').set(0)
      this.ref.detectChanges()
    })
    this.service.storage_get('lang').then((res) => {
      this.lang = res;
    })

  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.statePage = true
      this.service.fb_val('notification/user_' + this.user.member_id + '/alert', (state) => {
        if (this.statePage) {
          this.loadListAuction(this.user.member_id, 1)
        }
      })
    }, 500);
  }

  open(noti) {
    console.log('clicked open');
    console.log(noti);
    
    if (noti.actions == '1') {
      this.youtube.openVideo(noti.content);
    } else {
      this.showdetails(noti.notisys_id);
    }

  }
  public havealert = false
  public listAlert: any = []
  loadListAuction(member_id, page) {
    this.service.postData('getNotiSys', { member_id: member_id, page: page }).then((auct_data: []) => {
      console.log(auct_data);
      if (auct_data.length != 0) {
        this.havealert = true
      } else {
        this.havealert = false
      }
      this.listAlert = auct_data
      console.log(this.listAlert);
      this.ref.detectChanges()
    })

  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
  }
  ionViewDidLeave() {
    this.statePage = false
    this.refresher.disabled = true;
  }

  async doRefresh(event) {
    await this.loadListAuction(this.user.member_id, 1)
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }
  moreNoti(event) {
    this.page = Math.ceil(this.listAlert.length / 10) + 1
    console.log(this.page);
    this.service.postData('getNotiSys', { member_id: this.user.member_id, page: this.page }).then((auct_data: []) => {
      console.log(auct_data);
      auct_data.forEach(element => {
        this.listAlert.push(element)
      });
      setTimeout(() => {
        event.target.complete();
      }, 500);
    })
  }
  public order: any
  public data = {
    order_id: '3'
  }
  async showdetails(noti_id) {
    let data = {
      noti_id: noti_id
    }
    const modal = await this.modalController.create({
      component: NotiDetailsPage,
      cssClass: 'my-custom-class',
      componentProps: data
    });
    modal.onDidDismiss().then(()=>{
      this.loadListAuction(this.user.member_id, 1)
    })
    return await modal.present();
  }
}
