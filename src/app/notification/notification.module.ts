import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotificationPageRoutingModule } from './notification-routing.module';

import { NotificationPage } from './notification.page';
import { Routes, RouterModule, RouterLinkActive } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AuctionCongretePageModule } from '../auction-congrete/auction-congrete.module';
import { AuctionCongretePage } from '../auction-congrete/auction-congrete.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    NotificationPageRoutingModule,
    AuctionCongretePageModule,
  ],
  declarations: [NotificationPage],
  entryComponents:[AuctionCongretePage]
})
export class NotificationPageModule { }
