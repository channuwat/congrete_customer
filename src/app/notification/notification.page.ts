import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, RouterLinkActive, ActivatedRoute } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
  selected = 0;
  constructor(
    public router: Router, 
    public activatedRoute: ActivatedRoute,
    public api: WebapiServiceProvider,
    public fb: AngularFireDatabase,
    public translate: TranslateService,
    private storage: Storage,
    public ref: ChangeDetectorRef
    ) { }
    public login={
      member_id:''
    }
  ngOnInit() {
    this.api.storage_get('data_login').then((login) => {
      this.login=login;
      console.log(login);
      
      // this.load_noti();
    })

  }
  ionViewWillEnter() {
    this.fb.database.ref('notification/user_'+this.login.member_id+'/alert').set(0)
    this.selected = 0;
      this.ref.detectChanges()
  }
  page(uri) {
    this.router.navigate(['/main/notification/' + uri]);
    if (uri == 'auction') {
      this.selected = 0;
    } else if (uri == 'noti-system') {
      this.selected = 1;
    }
  }

  load_noti(){
    this.api.getData('getListAlerAuction/'+15).then((res:any)=>{
      console.log(res);
      
    })
  }
  
}
