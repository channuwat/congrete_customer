import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { AngularFireDatabase } from '@angular/fire/database';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { IonRefresher, ModalController } from '@ionic/angular';
import { AuctionCongretePage } from 'src/app/auction-congrete/auction-congrete.page';

@Component({
  selector: 'app-auction',
  templateUrl: './auction.page.html',
  styleUrls: ['./auction.page.scss'],
})
export class AuctionPage implements OnInit {
  @ViewChild(IonRefresher, { static: false }) refresher: IonRefresher;
  constructor(
    public service: WebapiServiceProvider,
    public fb: AngularFireDatabase,
    public translate: TranslateService,
    public ref: ChangeDetectorRef,
    public router: Router,
    public modalController: ModalController,
  ) { }
  public user = {
    member_id: ''
  }
  public statePage = false;
  public lang = 'th';
  page = 1;
  ngOnInit() {
    this.service.storage_get('data_login').then((data_u: any) => {
      this.user = data_u;
      this.fb.database.ref('notification/user_' + this.user.member_id + '/alert').set(0)
      this.ref.detectChanges();
    })
    this.service.storage_get('lang').then((res) => {
      this.lang = res;
    })

  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.statePage = true
      this.service.fb_val('notification/user_' + this.user.member_id + '/alert', (state) => {
        if (this.statePage) {
          this.loadListAuction(this.user.member_id, 1)
        }
      })
    }, 500);
  }

  public havealert = false
  public listAlert: any = []
  loadListAuction(member_id, page) {
    this.service.postData('getNoti', { member_id: member_id, page: page }).then((auct_data: []) => {
      console.log(auct_data);
      if (auct_data.length != 0) {
        this.havealert = true
      } else {
        this.havealert = false
      }
      this.listAlert = auct_data
      console.log(this.listAlert);
      this.ref.detectChanges()
    })

  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
  }
  ionViewDidLeave() {
    this.statePage = false
    this.refresher.disabled = true;
  }
  async doRefresh(event) {
    await this.loadListAuction(this.user.member_id, 1)
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }
  moreNoti(event) {
    this.page = Math.ceil(this.listAlert.length / 10) + 1
    console.log(this.page);
    this.service.postData('getNoti', { member_id: this.user.member_id, page: this.page }).then((auct_data: []) => {
      console.log(auct_data);
      auct_data.forEach(element => {
        this.listAlert.push(element)
      });
      setTimeout(() => {
        event.target.complete();
      }, 500);
    })
  }
  public order: any
  public data = {
    order_id: '3'
  }
  async showdetails(order_id) {
    this.service.postData('readNoti', { order_id: order_id, member_id: this.user.member_id }).then(() => {
      this.loadListAuction(this.user.member_id, 1);
      this.fb.database.ref('notification/user_' + this.user.member_id + '/alert').set(0)
      this.ref.detectChanges();
    })
    let data = {
      order_id: order_id
    }
    const modal = await this.modalController.create({
      component: AuctionCongretePage,
      componentProps: data
    });
    return await modal.present();
  }

}
