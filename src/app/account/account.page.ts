import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { ActionSheetController, ModalController, LoadingController, IonRefresher } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { TranslateService, DefaultLangChangeEvent } from '@ngx-translate/core';
import { ConfigAccountPage } from '../config-account/config-account.page';
import { ScorePage } from '../score/score.page';
import { Device } from '@ionic-native/device/ngx';
import { async } from '@angular/core/testing';
import { AddPasswordPage } from '../add-password/add-password.page';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  @ViewChild(IonRefresher, { static: false }) refresher: IonRefresher;
  constructor(public router: Router,
    public translate: TranslateService,
    public actionSheetController: ActionSheetController,
    private ref: ChangeDetectorRef,
    public store: Storage,
    public loadingController: LoadingController,
    public modalController: ModalController,
    public device: Device,
    public api: WebapiServiceProvider) { }
  public data = { member_id: 0, phone: '', name: '', email: '', address: '', picture: '' };
  public score = 0;
  public score_details: any;
  ngOnInit() {
  }

  ionViewWillEnter() {
    this.api.storage_get('data_login').then((data: any) => {
      this.data = data;
      this.load_details(this.data.member_id);
      this.api.getData('get_score/' + this.data.member_id).then((res: any) => {
        console.log(res);
        this.score = res.score;
        this.ref.detectChanges()
      })
    });
  }

  async change_lang() {
    let CHOOSELANGUAGE = 'เลือกภาษา'
    let CLOSE = 'ปิดออก'
    this.translate.get('CHOOSELANGUAGE').subscribe(
      value => {
        CHOOSELANGUAGE = value;
      }
    )
    this.translate.get('CLOSE').subscribe(
      value => {
        CLOSE = value;
      }
    )
    const actionSheet = await this.actionSheetController.create({
      header: CHOOSELANGUAGE,
      mode:'ios',
      buttons: [{
        text: 'ไทย',
        role: 'destructive',
        // icon: 'trash',
        handler: () => {
          this.api.storage_get('lang').then((res) => {
            if (res != 'th') {
              this.translate.use('th')
              this.api.storage_set('lang', 'th')
              this.presentLoading();
            }
          })
        }
      }, {
        text: 'English',
        // icon: 'share',
        handler: () => {
          this.api.storage_get('lang').then((res) => {
            if (res != 'en') {
              this.translate.use('en')
              this.api.storage_set('lang', 'en')
              this.presentLoading();
            }
          })
        }
      }, {
        text: CLOSE,
        // icon: 'close',
        role: 'cancel',
        handler: () => {
        }
      }]
    });
    await actionSheet.present();
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000

    });
    await loading.present();
    location.reload()
    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
  async presentLogout() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'กำลังออกจากระบบ...'

    });
    await loading.present();
    this.api.storage_set('data_login', null);
    this.api.storage_set('fbuser', null);
    this.api.storage_set("step1", null);
    this.api.storage_set("step2", null);
    this.api.storage_set("step21", null);
    let uuid = this.device.uuid;
    this.api.getData('logout_device/' + uuid).then(async () => {
      await loading.dismiss();
      setTimeout(() => {
        this.router.navigate(['/auth/login']);
      }, 100);
    })
  }

  logout() {

    this.presentLogout()

  }
  load_details(member_id) {
    this.api.getData('score_details/' + member_id).then((res: any) => {
      console.log(res);
    })
  }
  async change_password() {
    const modal = await this.modalController.create({
      component: AddPasswordPage
    })
    return await modal.present();
  }

  change_password1() {
    this.store.set('otp_phone', this.data.phone)
    setTimeout(() => {
      this.router.navigate(['../../../auth/add-password']);
    }, 500);

  }
  async doRefresh(event) {
    await this.ionViewWillEnter();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }

  async continue(member_id) {
    const modal = await this.modalController.create({
      component: ConfigAccountPage,
      componentProps: { member_id: member_id }
    });
    modal.onDidDismiss().then(()=>{
      this.ionViewWillEnter()
    })
    return await modal.present();
  }
  async details(member_id) {
    console.log(member_id);
    const modal = await this.modalController.create({
      component: ScorePage,
      componentProps: { member_id: member_id }
    });
    return await modal.present();
  }
  contact() {
    this.router.navigate(['/main/tab3']);
  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
  }
  ionViewWillLeave() {
    this.refresher.disabled = true;
  }

}
