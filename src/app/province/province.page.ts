import { Component, OnInit } from '@angular/core';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-province',
  templateUrl: './province.page.html',
  styleUrls: ['./province.page.scss'],
})
export class ProvincePage implements OnInit {
  public provice_server = [];
  public province = [];
  constructor(public api: WebapiServiceProvider, public modal: ModalController) { }
  public lang='th'
  ngOnInit() {
    this.api.storage_get('lang').then((res:any)=>{
      this.lang=res
    })
    this.api.getData("get_province").then((res: any) => {
      this.provice_server = res;
      this.province = res;
    })
  }

  getItems(ev: any) {
    const val = ev.target.value;
    this.province = this.provice_server.filter(item => {
      return item.name_th.indexOf(val) > -1;
    });
  }
  select(p) {
    console.log(p);
    this.modal.dismiss(p);
  }
}
