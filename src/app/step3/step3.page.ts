import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.page.html',
  styleUrls: ['./step3.page.scss'],
})
export class Step3Page implements OnInit {
  public step1 = { receive: true, receive2: false, address: '', receive_name: '', receive_phone: '' };
  public step2 = { p_name: '', p_img: '' };
  public step21 = { date: '', time: '', count: 0 };
  public data = {
    contact: '',
    phone: '',
    address: ''
  };
  public last_order = 0;
  constructor(public router: Router,
    public api: WebapiServiceProvider,
    public loadingController: LoadingController,
    private ref: ChangeDetectorRef,
    public translate: TranslateService,
  ) {

  }

  ngOnInit() {
    this.api.storage_get("step1").then((step: any) => {
      this.api.storage_get("lang").then((res: any) => {
        if (res == 'en') {
          this.step1 = step;
          this.data.address = this.step1.address + ", " + step.subdistrict_one.name_en + ", " + step.district_one.name_en + ", " + step.province.name_en;
          if (this.step1.receive) {
            this.api.storage_get('data_login').then((login: any) => {
              this.data.contact = login.name;
              this.data.phone = login.phone;
            })
          } else {
            this.data.contact = this.step1.receive_name;
            this.data.phone = this.step1.receive_phone;
          }
        } else {
          this.step1 = step;
          this.data.address = this.step1.address + " ต." + step.subdistrict_one.name_th +  " อ." + step.district_one.name_th +" จ." + step.province.name_th;
          if (this.step1.receive) {
            this.api.storage_get('data_login').then((login: any) => {
              this.data.contact = login.name;
              this.data.phone = login.phone;
            })
          } else {
            this.data.contact = this.step1.receive_name;
            this.data.phone = this.step1.receive_phone;
          }

        }
      })

    });
    this.api.storage_get("step2").then((step) => {
      this.step2 = step;
    })
    this.api.storage_get("step21").then((step) => {
      this.step21 = step;
    });


  }
  back() {
    this.router.navigate(['/main/step2-1']);
  }
  async next() {
    const loading = await this.loadingController.create({
      message: 'กำลังส่งคำขอของคุณ...'
    });
    await loading.present();
    this.api.storage_get('data_login').then((login) => {
      this.api.postData('add_order', { step1: this.step1, step2: this.step2, step21: this.step21, user: login }).then((res: any) => {
        console.log(res);
        this.last_order = res.last_order;
        loading.dismiss();
        this.api.storage_set("step1", null);
        this.api.storage_set("step2", null);
        this.api.storage_set("step21", null);
        this.api.fb_set('order');
        this.api.fb_set('order_new');
        this.router.navigate(['/main/order-success/' + this.last_order]);

      }, (err) => {
        console.log(err);
        console.log("error");
      });
    });


  }
  async doRefresh(event) {
    await this.ngOnInit();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }
  ionViewWillEnter() {
    this.api.storage_get('step1').then((res: any) => {
      if (res == null) {
        this.data = {
          contact: '',
          phone: '',
          address: ''
        };
      } else {
        this.api.storage_get("step1").then((step: any) => {
          this.api.storage_get("lang").then((res: any) => {
            if (res == 'en') {
              this.step1 = step;
              this.data.address = this.step1.address + ", " + step.district_one.name_en + ", " + step.subdistrict_one.name_en + ", " + step.province.name_en;
              if (this.step1.receive) {
                this.api.storage_get('data_login').then((login: any) => {
                  this.data.contact = login.name;
                  this.data.phone = login.phone;
                })
              } else {
                this.data.contact = this.step1.receive_name;
                this.data.phone = this.step1.receive_phone;
              }
            } else {
              this.step1 = step;
              this.data.address = this.step1.address + " ต." + step.district_one.name_th + " อ." + step.subdistrict_one.name_th + " จ." + step.province.name_th;
              if (this.step1.receive) {
                this.api.storage_get('data_login').then((login: any) => {
                  this.data.contact = login.name;
                  this.data.phone = login.phone;
                })
              } else {
                this.data.contact = this.step1.receive_name;
                this.data.phone = this.step1.receive_phone;
              }
    
            }
          })
        });
        this.api.storage_get("step2").then((step) => {
          this.step2 = step;
        })
        this.api.storage_get("step21").then((step) => {
          this.step21 = step;
        });
        this.ref.detectChanges();
      }
    })
  }

}
