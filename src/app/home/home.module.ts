import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { HomePage } from './home.page';
import { AuctionCongretePageModule } from '../auction-congrete/auction-congrete.module';
import { AuctionCongretePage } from '../auction-congrete/auction-congrete.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    AuctionCongretePageModule,
    TranslateModule.forChild()
  ],
  declarations: [HomePage],
  entryComponents:[AuctionCongretePage]
})
export class HomePageModule {}
