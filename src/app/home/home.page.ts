import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { IonRefresher, ModalController } from '@ionic/angular';
import { NotificationDetailPage } from '../notification-detail/notification-detail.page';
import { AuctionCongretePage } from '../auction-congrete/auction-congrete.page';
import { TranslateService } from '@ngx-translate/core';
import { PaymentPage } from '../payment/payment.page';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild(IonRefresher, { static: false }) refresher: IonRefresher;
  public news = [];
  constructor(public router: Router, public api: WebapiServiceProvider,
    public translate: TranslateService,
    private ref: ChangeDetectorRef,
    public modalController: ModalController) {

  }
  public data = { member_id: 0, phone: '', name: '', email: '', address: '', picture: '' };
  public new_order = [];
  public date_now: any;
  public last_time: any = -1;
  public lang = 'th'
  public fbstatus = false;
  ngOnInit() {
    this.fbstatus = true;
    this.api.storage_get('lang').then((res: any) => {
      this.lang = res;
    })
    console.log('route url : ' + this.router.url);
    console.log('ngOnInit');
    this.api.fb_val('order', (val) => {
      if (this.fbstatus) {
        this.api.storage_get('data_login').then((data: any) => {
          if (data) {
            console.log(data);
            this.data = data;
            this.api.getData("order_data/" + data.member_id + "/30/1").then((res: any) => {
              this.new_order = res;
              console.log(res);
              this.ref.detectChanges();
            });
          }
        });
      }
    })
    this.date_now = new Date();
    this.load_news();
  }

  async open_detail(n) {
    const modal = await this.modalController.create({
      component: NotificationDetailPage,
      cssClass: 'my-custom-class',
      componentProps: { data: n }
    });
    return await modal.present();
  }

  load_news() {
    this.api.getData("load_news/" + this.lang).then((res: any) => {
      this.news = res;
      console.log(res);

    });
  }
  order_now() {
    this.router.navigate(['/main/step1']);
  }
  all_order() {
    this.router.navigate(['/main/tab2/order-auction']);
  }
  async openAuction(order_id) {
    if (order_id.status == 20 || order_id.status == -2) {
      let order = order_id.order_id;
      const modal = await this.modalController.create({
        component: PaymentPage,
        componentProps: { order: order }
      });
      return await modal.present();

    } else {
      const modal = await this.modalController.create({
        component: AuctionCongretePage,
        componentProps: order_id
      });
      return await modal.present();
    }
  }
  async doRefresh(event) {
    await this.ngOnInit()
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
    this.ngOnInit();
  }
  ionViewWillLeave() {
    this.refresher.disabled = true;
    this.fbstatus = false;
  }
}
