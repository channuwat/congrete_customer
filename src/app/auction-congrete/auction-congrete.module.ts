import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuctionCongretePageRoutingModule } from './auction-congrete-routing.module';

import { AuctionCongretePage } from './auction-congrete.page';
import { ComponentsModule } from '../components/components.module';
import { PaymentPageModule } from '../payment/payment.module';
import { PaymentPage } from '../payment/payment.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AuctionCongretePageRoutingModule,
    TranslateModule.forChild(),
    PaymentPageModule
  ],
  declarations: [AuctionCongretePage],
  entryComponents:[PaymentPage]
})
export class AuctionCongretePageModule {}
