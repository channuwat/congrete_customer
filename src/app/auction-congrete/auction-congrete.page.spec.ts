import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AuctionCongretePage } from './auction-congrete.page';

describe('AuctionCongretePage', () => {
  let component: AuctionCongretePage;
  let fixture: ComponentFixture<AuctionCongretePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuctionCongretePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AuctionCongretePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
