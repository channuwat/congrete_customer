import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { NavParams, ModalController } from '@ionic/angular';
import { PaymentPage } from '../payment/payment.page';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-auction-congrete',
  templateUrl: './auction-congrete.page.html',
  styleUrls: ['./auction-congrete.page.scss'],
})
export class AuctionCongretePage implements OnInit {
  @Input() data: any;
  constructor(
    private socialSharing: SocialSharing,
    public alertController: AlertController,
    public translate: TranslateService,
    public modal: ModalController,
    public NavParams: NavParams,
    public route: ActivatedRoute,
    public api: WebapiServiceProvider,
    public ref: ChangeDetectorRef,
    public router: Router,
    public modalController: ModalController,
  ) {
    this.api.storage_get('lang').then((res: any) => {
      if (res == 'en') {
        this.lang = 'en'
      } else {
        this.lang = 'th'
      }
    })
  }
  public lang = 'th'
  public winner = {
    shipping: 0,
    sub: 0,
    sumprice: 0,
    ap_id: '',
    name: '',
    phone: '',
    sale_id: '',
    ap_price: 0,
    priceperq: 0,
    vat: 0,
    novat:0,
    content:'แชร์เพื่อรับคะแนนเพิ่ม'
  }
  public fbstatus = false;
  public bank: any;
  public order: any;
  public date_now: any;
  ngOnInit() {
    this.order = this.NavParams.get('order_id')
    this.date_now = new Date();
  }
  share(order_id) {
    let message = 'คอนกรีตเดลิเวอรี่ ดาวน์โหลดได้แล้ว ที่'
    let subject = 'มาลองใช้กัน'
    let url = 'https://concretedeliveryeasy.com/social/?ref='+this.od_head.member_id+'?order='+order_id;
    this.socialSharing.share(message,subject,null,url).then((res) => {
      // this.api.Toast('แชร์เรียบร้อย')
      // this.api.getData('sharePoint/'+order_id+'/'+this.od_head.member_id).then((res)=>{
      //   if (res == 'OK') {
      //     this.api.Toast('ได้รับ 100 คะแนน จากการแชร์เรียบร้อย');
      //   }
      // })
     }).catch(() => {
     });
  }

  ionViewWillEnter() {
    // this.loadAuctionList();
    this.fbstatus = true;
    this.api.fb_val('order', (res: any) => {
      if (this.fbstatus) {
        this.loadAuctionList();
      }
    })

    this.setTimeOut()
  }

  setTime() {
    let date = new Date()
    let d: any = date.getDate()
    if (d < 10) {
      d = '0' + d
    }
    let m: any = date.getMonth() + 1
    if (m < 10) {
      m = '0' + m
    }
    let y: any = date.getFullYear()
    let h: any = date.getHours()
    let min: any = date.getMinutes()
    let s: any = date.getSeconds()
    return y + '-' + m + '-' + d + ' ' + h + ':' + min + ':' + s
  }

  public od_head = {
    choose: 0,
    member_id: '',
    p_name: { th: '', en: '' },
    order_id: '',
    status: 0,
    car: '',
    send_date: '01-01-2020',
    send_time: '00:00:00',
    name: '',
    count: 0,
    address: '',
    district_th: '',
    subdistrict_th: '',
    province_th: '',
    district_en: '',
    subdistrict_en: '',
    province_en: '',
    memberaddress: '',
    phone: '',
    subdistrict: '',
    receive: '1',
    receive_name: '',
    cre_date: '',
    receive_phone: '',
    ap_price: '',
    zip_code:''
  };
  public od_auction = []
  public od_auction_pre = {
    name: '-',
    ap_price: 0,
    ap_diff_price: 0,
    create_date: '',
    sumprice: 0,
    send_date: '',
    send_time: '',
    count: 0,
    car: ''
  }
  load_bank() {
    this.api.getData('load_bank').then((res: any) => {
      this.bank = res;
    })
  }

  loadAuctionList() {
    this.api.getData('get_vs_auction/' + this.order).then((data: any) => {
      if (data.order_data != '') {
        this.od_head = data.order_data

        if (data.order_data.status == 0 && data.order_data.choose == 0) {
          this.time = data.order_data.end_date
        } else {
          this.time = null
        }
        this.load_winner(data.order_data.order_id);
        this.ref.detectChanges();
      }
      if (data.auction_rec) {
        this.od_auction = data.auction_rec
        if (this.od_auction.length > 0) {
          this.od_auction_pre = this.od_auction[0]
        }
        this.ref.detectChanges();
      }

    })
  }

  public time = null
  setTimeOut() {
    let int_dateE = Date.parse(this.time)
    let int_dateN = Date.parse(this.setTime())
    if (int_dateN > int_dateE) {
      this.time = null
    }
    setInterval(() => {
      int_dateE = Date.parse(this.time)
      int_dateN = Date.parse(this.setTime())
      if (int_dateN > int_dateE) {
        this.time = null
      }
    }, 1000);
  }
  accept(order_id) {
    this.api.postData('confirm/', { order_id: order_id, sale_id: this.winner.sale_id, ap_id: this.winner.ap_id, member_id: this.od_head.member_id }).then((res: any) => {
      this.api.fb_set('order');
      this.loadAuctionList();
      this.api.Toast('ได้รับ ' + res + ' คะแนน ตรวจสอบได้ที่บัญชีของฉัน')

    })
  }
  async continue(order) {
    this.close()
    const modal = await this.modalController.create({
      component: PaymentPage,
      componentProps: { order: order }
    });
    return await modal.present();
  }
  ngOnDestroy() {
    this.fbstatus = false;
  }
  close() {
    this.modal.dismiss()
  }
  async doRefresh(event) {
    await this.ngOnInit();
    await this.ionViewWillEnter();
    setTimeout(async () => {
      await event.target.complete();
    }, 500);
  }
  load_winner(order_id) {
    this.api.getData('who_won/' + order_id).then((res: any) => {
      this.winner = res;
      this.winner.vat = +((res.sumprice * 7) / 100).toFixed(2)
      this.winner.shipping = ((res.shipping).toFixed(2))
      this.winner.sub = +(res.sumprice - res.shipping).toFixed(2);
      this.winner.novat = +(res.sumprice - this.winner.vat).toFixed(2);
    })
  }
  public copyMessage(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.api.Toast('คัดลอกหมายเลข ' + val)
  }

  // }
  async final(ap) {
    let title = 'ปิดประมูล';
    let THB = 'บาท';
    let body1 = 'ต้องการปิดประมูลก่อนเวลา';
    let body2 = 'รายการจะสิ้นสุดการประมูลทันที';
    let at = 'ที่ราคา';
    let ok = 'ยืนยัน';
    let cancel = 'ยกเลิก';
    this.translate.get('CLOSEAUC').subscribe(
      value => {
        title = value;
      }
    )
    this.translate.get('BODY1').subscribe(
      value => {
        body1 = value;
      }
    )
    this.translate.get('BODY2').subscribe(
      value => {
        body2 = value;
      }
    )
    this.translate.get('AT').subscribe(
      value => {
        at = value;
      }
    )
    this.translate.get('COMFIRM').subscribe(
      value => {
        ok = value;
      }
    )
    this.translate.get('CANCEL').subscribe(
      value => {
        cancel = value;
      }
    )
    this.translate.get('THB').subscribe(
      value => {
        THB = value;
      }
    )
    const alert = await this.alertController.create({
      cssClass: 'alert',
      mode: 'ios',
      header: title,
      message: body1 + ' <br> ' + at + ' <strong>' + ap.sumprice.toLocaleString('en-us') + '</strong> ' + THB + ' <br> ' + body2,
      buttons: [
        {
          text: cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: ok,
          handler: () => {
            this.api.postData('final/', { ap_id: ap.ap_id, order_id: this.od_head.order_id, now_date: Math.round(new Date().getTime() / 1000) }).then((res: any) => {
              this.api.fb_set('order');
              this.ngOnInit();
              this.ionViewWillEnter();
            })
          }
        }
      ]
    });

    await alert.present();
  }

}
