import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuctionCongretePage } from './auction-congrete.page';

const routes: Routes = [
  {
    path: '',
    component: AuctionCongretePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuctionCongretePageRoutingModule {}
