import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { Platform } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: [
    './styles/login.page.scss'
  ]
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  public Phonerequired = 'Phone is required.';
  public EnterPhone = 'Enter a valid Phone.';
  public PhoneLeast = 'Phone must be at least 9 characters long.';
  public PhoneLargest = 'Phone must be at largest  10 characters long.';
  public Passwordrequired = 'Password is required.';
  public PasswordLeast = 'Password must be at least 6 characters long.';
  validation_messages = {
    'tel': [
      { type: 'required', message: this.Phonerequired },
      { type: 'pattern', message: this.EnterPhone },
      { type: 'minlength', message: this.PhoneLeast },
      { type: 'maxlength', message: this.PhoneLargest }
    ],
    'password': [
      { type: 'required', message: this.Passwordrequired },
      { type: 'minlength', message: this.PasswordLeast }
    ]
  };

  constructor(
    public router: Router,
    public menu: MenuController,
    public fb: Facebook,
    public api: WebapiServiceProvider,
    private storage: Storage,
    private platform: Platform,
    public device: Device,
    public fcm: FCM,
    public firebase: AngularFireDatabase,
    public translate: TranslateService,
  ) {
    this.tranSub()
    this.loginForm = new FormGroup({
      'tel': new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(9), Validators.maxLength(10),
        Validators.pattern('[0-9]+')
      ])),
      'password': new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ]))
    });
  }
  tranSub() {
    this.translate.get('Phonerequired').subscribe(
      value => {
        this.Phonerequired = value;
      }
    )
    this.translate.get('EnterPhone').subscribe(
      value => {
        this.EnterPhone = value;
      }
    )
    this.translate.get('PhoneLeast').subscribe(
      value => {
        this.PhoneLeast = value;
      }
    )
    this.translate.get('PhoneLargest').subscribe(
      value => {
        this.PhoneLargest = value;
      }
    )
    this.translate.get('Passwordrequired').subscribe(
      value => {
        this.Passwordrequired = value;
      }
    )
    this.translate.get('PasswordLeast').subscribe(
      value => {
        this.PasswordLeast = value;
        console.log(value);
        
      }
    )
    this.validation_messages = {
      'tel': [
        { type: 'required', message: this.Phonerequired },
        { type: 'pattern', message: this.EnterPhone },
        { type: 'minlength', message: this.PhoneLeast },
        { type: 'maxlength', message: this.PhoneLargest }
      ],
      'password': [
        { type: 'required', message: this.Passwordrequired },
        { type: 'minlength', message: this.PasswordLeast }
      ]
    };
   
  }

  ngOnInit(): void {
    this.menu.enable(false);
  }
  public userDeviceData = { uuid: "", token: "", platform: "", version: "", member_id: '0' };
  public userDeviceResponse: any = {
    Sucess: ''
  }

  doLogin(): void {
    this.api.postData("login", this.loginForm.value).then((res: any) => {
      console.log(res);
      console.log(this.platform.is('mobileweb'));
      console.log(this.fcm.getToken());

      if (res.flag == '1') {
        this.api.storage_set("data_login", res.data);
        this.api.Toast("ข้อมูลถูกต้อง กำลังเข้าสู่ระบบ");
        console.log(!this.platform.is('pwa'));
        console.log(!this.platform);
        if (!this.platform.is('pwa')) {


          this.fcm.subscribeToTopic('all');
          this.fcm.getToken().then(token => {

            this.userDeviceData.token = token;
            // Device info
            this.userDeviceData.uuid = this.device.uuid;
            this.userDeviceData.platform = this.device.platform;
            this.userDeviceData.version = this.device.version;
            console.log(this.userDeviceData);

            this.userDeviceData.member_id = res.data.member_id;
            this.api.postData('register_device', this.userDeviceData).then((result) => {
              setTimeout(() => {
                this.router.navigate(['main/home']);
              }, 1000);
              this.userDeviceResponse = result;
              if (this.userDeviceResponse.Sucess) {
                // this.api.Toast('Regis new device success');
                // Keep UUID to localStorage
                this.storage.set('uuid', this.device.uuid);
              } else {
                // this.api.Toast('Fail! Cannot Regis new device');
              }
            }, (error) => {
              //console.log(error);
              if (error.status == 0) {
                // this.api.Toast('Error status Code:0 Web API Offline');
              }
            });
            // Connect Web API
          }); // then
        } else {
          this.router.navigate(['main/home']);
        }
      } else {
        this.api.Toast("เบอร์โทรศัพท์ หรือ รหัสผ่านไม่ถูกต้อง");
      }
    })
  }
  goToForgotPassword(): void {
    this.router.navigate(['main/home']);
  }
  doFacebookLogin(): void {
    let profile: any;
    this.fb.login(['public_profile', 'email'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res);
        this.getUserDetail(res.authResponse.userID);

      })
      .catch(e => console.log('Error logging into Facebook', e));
    // this.router.navigate(['main/home']);
    // this.api.Toast(profile)
  }
  doGoogleLogin(): void {
    this.router.navigate(['main/home']);
  }

  doTwitterLogin(): void {
    this.router.navigate(['main/home']);
  }
  public users = {
    id: '',
    email: '',
    name: '',
    picture: ''
  }
  getUserDetail(userid: any) {
    this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
      .then((res: any) => {
        console.log(res);
        let picture = "https://graph.facebook.com/" + res.id + "/picture?width=512&height=512";
        this.users = res;
        res.picture = picture;
        this.api.storage_set('fbuser', res)
        this.api.postData('fblogin', { id: res.id, email: res.email, name: res.name, picture: picture }).then((res: any) => {
          let fbstatus: any = res;
          if (res.flag == '1') {
            this.api.storage_set("data_login", res.data);
            if (!this.platform.is('desktop')) {
              this.fcm.subscribeToTopic('all');
              this.fcm.getToken().then(token => {

                this.userDeviceData.token = token;
                // Device info
                this.userDeviceData.uuid = this.device.uuid;
                this.userDeviceData.platform = this.device.platform;
                this.userDeviceData.version = this.device.version;
                console.log(this.userDeviceData);

                this.userDeviceData.member_id = res.data.member_id;
                this.api.postData('register_device', this.userDeviceData).then((result) => {
                  this.api.Toast('กำลังเข้าสู่ระบบ');
                  setTimeout(() => {
                    this.router.navigate(['main/home']);
                  }, 1000);
                  this.userDeviceResponse = result;
                  if (this.userDeviceResponse.Sucess) {

                    // this.api.Toast('Regis new device success');
                    // Keep UUID to localStorage
                    this.storage.set('uuid', this.device.uuid);
                  } else {
                    // this.api.Toast('Fail! Cannot Regis new device');
                  }
                }, (error) => {
                  //console.log(error);
                  if (error.status == 0) {
                    // this.api.Toast('Error status Code:0 Web API Offline');
                  }
                });
                // Connect Web API
              }); // then
            } else {
              this.router.navigate(['main/home']);
            }
          } else {
            // New facebook user login
            this.router.navigate(['auth/signup-phone'])
          }
        })
      })
      .catch(e => {
        console.log(e);
      });
  }
  register() {
    this.router.navigate(['auth/signup-phone'])
  }
  public fboption = false;
  ionViewWillEnter() {
    this.tranSub()
    this.storage.get('walkthrough').then((res: any) => {
      if (!res) {
        this.router.navigate(['walkthrough']);
      }
    })
    this.api.getData('loginoptions').then((res: any) => {
      if (res) {
        this.fboption = res;
      }

    })
  }
  ngOnDestroy() {
    this.api.storage_set("otp_code", null);
    this.api.storage_set("otp_phone", null);
    this.api.storage_get('data_login').then((res: any) => {
      if (res) {
        setTimeout(() => {
          location.reload()
        }, 200);
      }
    })
  }

}
